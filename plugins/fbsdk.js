/* eslint-disable no-undef */
/* eslint-disable camelcase */
import Vue from 'vue'

const vue_fb = {}
vue_fb.install = function install(Vue, options) {
  ;(function(d, s, id) {
    const fjs = d.getElementsByTagName(s)[0]
    if (d.getElementById(id)) {
      return
    }
    const js = d.createElement(s)
    js.id = id
    js.src = '//connect.facebook.net/en_US/sdk.js'
    fjs.parentNode.insertBefore(js, fjs)
    console.log('setting fb sdk')
  })(document, 'script', 'facebook-jssdk')
  window.fbAsyncInit = function onSDKInit() {
    FB.init(options)
    FB.AppEvents.logPageView()
    Vue.FB = FB
    vue_fb.sdk = FB // do not forget this line
    window.dispatchEvent(new Event('fb-sdk-ready'))
  }
  Vue.FB = undefined
}

Vue.use(vue_fb, {
  appId: 537251177021584,
  autoLogAppEvents: true,
  xfbml: true,
  version: 'v5.0'
})
// and this line
export default ({ app }, inject) => {
  inject('fb', vue_fb)
}
