import Vue from 'vue'
import 'vue2-daterange-picker/dist/vue2-daterange-picker.css'

Vue.component('date-range-picker', require('vue2-daterange-picker').default)
