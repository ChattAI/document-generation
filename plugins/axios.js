/* eslint-disable no-console */
import { getToken, unSetToken } from '~/utils/auth'

export default function({ $axios, redirect, store, route, app }) {
  $axios.onRequest((config) => {
    const token = getToken()
    if (token) {
      config.headers.Authorization = `token ${token}`
    }
    return config
  })

  $axios.onRequestError((err) => {
    console.log('Making request error ' + err)
  })

  $axios.onResponse((response) => {
    // Any status code that lie within the range of 2xx cause this function to trigger
    if (response.data.code === 403 && response.data.msg === 'Unauthorized') {
      store.dispatch('resetSession').then()
      unSetToken()
      const path = app.localePath({
        name: 'login',
        query: { redirect: route.path }
      })
      redirect(path)
    }
    if (response.request.responseURL.includes('export')) {
      return response
    }
    return response.data
  })

  // $axios.onResponseError((err) => {
  //   // Any status codes that falls outside the range of 2xx
  //   const code = parseInt(err.response && err.response.status)
  //   if (code === 400) {
  //     redirect('/400')
  //   }
  // })

  //   $axios.onError((error) => {
  //     console.log('onError', error)
  //     // const code = parseInt(error.response && error.response.status)
  //     // if (code === 400) {
  //     //   redirect('/400')
  //     // }
  //   })
}
