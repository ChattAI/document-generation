// Include Dependencies
import Vue from 'vue'
import VueFusionCharts from 'vue-fusioncharts'
import FusionCharts from 'fusioncharts'
import Column2D from 'fusioncharts/fusioncharts.charts'
import FusionTheme from 'fusioncharts/themes/fusioncharts.theme.fusion'
import Powercharts from 'fusioncharts/fusioncharts.powercharts'

Vue.use(VueFusionCharts, FusionCharts, Column2D, FusionTheme, Powercharts)
