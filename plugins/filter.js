import Vue from 'vue'

Vue.filter('formatNumber', function(number) {
  return number.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.')
})
