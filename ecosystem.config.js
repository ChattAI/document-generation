module.exports = {
  apps: [
    {
      name: 'demo_new.chatt.ai',
      exec_mode: 'cluster',
      instances: 1, // Or a number of instances
      script: './node_modules/nuxt/bin/nuxt.js',
      args: 'start'
    }
  ]
}
