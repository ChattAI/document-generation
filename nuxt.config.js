import './env.config'
import path from 'path'
import fs from 'fs'

export default {
  mode: 'universal',
  server: {
    port: 3006,
    host: 'localhost',
    https:
      process.env.NODE_ENV === 'production'
        ? undefined
        : {
            key: fs.readFileSync(path.resolve(__dirname, 'localhost.key')),
            cert: fs.readFileSync(path.resolve(__dirname, 'localhost.crt'))
          }
  },
  /*
   ** Router config
   */
  router: {
    // Run the middleware/check-auth.js on every page
    middleware: 'check-auth'
  },
  dev: process.env.NODE_ENV !== 'production',
  /*
   ** Headers of the page
   */
  env: {
    ZALO_APP_ID: process.env.ZALO_APP_ID
  },
  head: {
    titleTemplate: '%s - ' + process.env.npm_package_name,
    title: process.env.npm_package_name || '',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      {
        hid: 'description',
        name: 'description',
        content: process.env.npm_package_description || ''
      }
    ],
    link: [{ rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }]
  },
  /*
   ** Customize the progress-bar color
   */
  loading: { color: '#fff', continuous: true },
  /*
   ** Global CSS
   */
  css: [],
  /*
   ** Plugins to load before mounting the App
   */
  plugins: [
    '~/plugins/axios',
    '~/plugins/vuex-persist.client',
    '~/plugins/filter.js',
    '~/plugins/date-range-picker.js',
    '~plugins/tooltip.js',
    '~plugins/timepickernew.js',
    { src: '~/plugins/vue-json-editor', mode: 'client' },
    { src: '~/plugins/chart', ssr: false },
    { src: '~/plugins/json-viewer.js', mode: 'client' },
    { src: '~/plugins/fbsdk.js', mode: 'client' }
  ],
  /*
   ** Nuxt.js dev-modules
   */
  buildModules: [
    // Doc: https://github.com/nuxt-community/eslint-module
    '@nuxtjs/eslint-module',
    // Doc: https://github.com/nuxt-community/stylelint-module
    '@nuxtjs/stylelint-module',
    '@nuxtjs/vuetify',
    '@nuxtjs/moment',
    'nuxt-socket-io'
  ],
  /*
   ** Nuxt.js modules
   */
  modules: [
    'nuxt-webfontloader',
    'nuxt-i18n',
    // Doc: https://axios.nuxtjs.org/usage
    '@nuxtjs/axios',
    '@nuxtjs/toast',
    '@nuxtjs/pwa',
    // Doc: https://github.com/nuxt-community/dotenv-module
    '@nuxtjs/proxy',
    [
      'nuxt-imagemin',
      {
        jpegtran: { progressive: true },
        optipng: { optimizationLevel: 7 },
        gifsicle: { optimizationLevel: 2 },
        pngquant: {
          quality: '65-90',
          speed: 4
        }
      }
    ],
    'nuxt-healthcheck'
  ],
  webfontloader: {
    google: {
      families: ['Noto+Sans:300,400,600,700&display=swap']
    }
  },
  i18n: {
    locales: [
      { code: 'vi', name: 'Tiếng Việt', file: 'vi.js' },
      { code: 'en', name: 'English', file: 'en.js' }
    ],
    defaultLocale: 'en',
    strategy: 'prefix_except_default',
    detectBrowserLanguage: false,
    lazy: true,
    langDir: 'lang/',
    vueI18n: {
      fallbackLocale: 'en'
    },
    vuex: {
      // Module namespace
      moduleName: 'i18n',
      // If enabled, current app's locale is synced with nuxt-i18n store module
      syncLocale: true,
      // If enabled, current translation messages are synced with nuxt-i18n store module
      syncMessages: false,
      // Mutation to commit to set route parameters translations
      syncRouteParams: true
    }
  },
  moment: {
    defaultLocale: 'vi',
    locales: ['vi'],
    timezone: false
  },
  /*
   ** Axios module configuration
   ** See https://axios.nuxtjs.org/options
   */
  axios: {
    proxy: true, // Can be also an object with default options,
    debug: true
  },
  proxy: {
    '/api/': {
      target: process.env.API_URL,
      pathRewrite: { '^/api/': '' }
    },
    '/api_spam/': {
      target: process.env.API_URL_SPAM,
      pathRewrite: { '^/api_spam/': '' }
    },
    '/api_autocomment/': {
      target: process.env.API_COMMENT,
      pathRewrite: { '^/api_autocomment/': '' }
    },
    '/api_analytics/': {
      target: process.env.API_ANALYTICS,
      pathRewrite: { '^/api_analytics/': '' }
    },
    '/api_crm/': {
      target: process.env.API_CRM,
      pathRewrite: { '^/api_crm/': '' }
    },
    '/api_conversation/': {
      target: process.env.API_CONVERSATION,
      pathRewrite: { '^/api_conversation/': '' }
    },
    '/api_authorization/': {
      target: process.env.API_AUTHORIZATION,
      pathRewrite: { '^/api_authorization/': '' }
    },
    '/api_passport/': {
      target: process.env.API_PASSPORT,
      pathRewrite: { '^/api_passport/': '' }
    },
    '/api_store/': {
      target: process.env.API_STORE,
      pathRewrite: { '^/api_store/': '' }
    }
  },
  /*
   ** vuetify module configuration
   ** https://github.com/nuxt-community/vuetify-module
   */
  vuetify: {
    customVariables: ['~/assets/variables.scss'],
    defaultAssets: {
      font: false
    },
    treeShake: true,
    optionsPath: './vuetify.options.js'
  },
  io: {
    sockets: [
      {
        name: 'support_onlive_chat',
        url: process.env.ONLIVE_CHAT_HOST
      }
    ]
  },
  /*
   ** Build configuration
   */
  toast: {
    position: 'top-center',
    className: 'toasted_',
    duration: 2000,
    iconPack: 'mdi',
    register: [
      {
        name: 'my_error',
        message: (payload) => {
          if (!payload.message) {
            return 'Oops.. Something Went Wrong..'
          }

          // if there is a message show it with the message
          return payload.message
        },
        options: {
          type: 'error',
          icon: 'mdi-close-circle-outline'
        }
      },
      {
        name: 'my_success',
        message: (payload) => {
          if (!payload.message) {
            return 'Success'
          }
          return payload.message
        },
        options: {
          type: 'success',
          icon: 'mdi-check-circle-outline'
        }
      }
    ]
  },
  build: {
    /*
     ** You can extend webpack config here
     */
    extend(config, ctx) {}
  }
}
