import Cookie from 'js-cookie'

export const setTokenCustomer = (user) => {
  if (process.SERVER_BUILD) return
  window.localStorage.setItem('customer', user)
  Cookie.set('customer', user)
}

export const unSetTokenCustomer = () => {
  if (process.SERVER_BUILD) return
  window.localStorage.removeItem('customer')
  Cookie.remove('customer')
}

export const getTokenCustomer = () => {
  if (process.SERVER_BUILD) return
  return Cookie.get('customer')
}

export const randomMessage = () => {
  let result = ''
  let characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz'
  for (let index = 0; index < 1000; index++) {
    characters += index
  }
  const charactersLength = characters.length
  for (let i = 0; i < 5; i++) {
    result += characters.charAt(Math.floor(Math.random() * charactersLength))
  }
  return result
}
