import moment from 'moment'
export const TypeAction = {
  botResComment: 'reply_comment',
  botResContent: 'inbox',
  botScenario: 'redirect_scenario',
  botOrder: 'human_handover',
  hideComment: 'no_type'
}

export const messTags = [
  { key: 'CONFIRMED_EVENT_UPDATE', name: 'Cập nhập sự kiện' },
  { key: 'POST_PURCHASE_UPDATE', name: 'Cập nhật sau mua hàng' },
  { key: 'ACCOUNT_UPDATE', name: 'Cập nhật tài khoản' }
]

export const itemTime = [
  { key: 1, name: 'Now' },
  { key: 2, name: 'Minute' },
  { key: 3, name: 'Hour' },
  { key: 4, name: 'Day' }
]

export const actionTypes = [
  { text: 'Văn bản', value: 'message', icon: 'svg/actions/text.svg' },
  // { text: 'Time typing', value: 'time_typing', icon: 'svg/actions/clock.svg' },
  { text: 'Image', value: 'picture', icon: 'svg/actions/image.svg' },
  { text: 'Slider', value: 'carousel', icon: 'svg/actions/slider.svg' },
  { text: 'Video', value: 'video', icon: 'svg/actions/video.svg' },
  {
    text: 'Trả lời nhanh',
    value: 'quick_replies',
    icon: 'svg/actions/quick_reply.svg'
  }
  // { text: 'contact with staff', value: 'handover' }
]

export const gerderType = {
  male: 'Nam',
  female: 'Nữ',
  other: 'Giới tính khác'
}

export const conditionFilter = [
  {
    key: 'like',
    name: 'Chứa'
  },
  {
    key: 'not_like',
    name: 'Không Chứa '
  },
  {
    key: 'is_empty',
    name: 'Trống'
  },
  {
    key: 'is_not_empty',
    name: 'Không trống '
  },
  {
    key: 'start_with',
    name: 'Bắt đầu bằng'
  },
  {
    key: 'end_with',
    name: 'Kết thúc bằng'
  }
]

export const defaultListFilterType = [
  {
    type: 'name',
    title: 'Tên khách hàng',
    name: '',
    condition: {
      key: 'like',
      name: 'Chứa'
    }
  },
  {
    type: 'phone_number',
    title: 'Số điện thoại',
    phone_number: '',
    condition: {
      key: 'like',
      name: 'Chứa'
    }
  },
  {
    type: 'email',
    title: 'Email',
    email: '',
    condition: {
      key: 'like',
      name: 'Chứa'
    }
  },
  {
    type: 'job_title',
    title: 'Công việc',
    job_title: '',
    condition: {
      key: 'like',
      name: 'Chứa'
    }
  },
  {
    type: 'gender',
    title: 'Giới tính',
    gender: {
      key: [],
      value: []
    },
    condition: {}
  },
  {
    type: 'uid',
    title: 'UID',
    uid: '',
    condition: {
      key: 'like',
      name: 'Chứa'
    }
  },
  {
    type: 'date_of_birth',
    title: 'Ngày sinh',
    condition: {}
  },
  {
    type: 'address',
    title: 'Địa chỉ',
    address: '',
    condition: {
      key: 'like',
      name: 'Chứa'
    }
  },
  {
    type: 'city',
    title: 'Tỉnh/Thành phố',
    city: '',
    condition: {
      key: 'like',
      name: 'Chứa'
    }
  },
  {
    type: 'relationship',
    title: 'Tình trạng mối quan hệ',
    relationship: 1,
    condition: {}
  },
  {
    type: 'characteristics',
    title: 'Đặc điểm nhận dạng',
    characteristics: '',
    condition: {
      key: 'like',
      name: 'Chứa'
    }
  },
  {
    type: 'channel',
    title: 'Nguồn khách hàng',
    channel: '',
    condition: {}
  },
  {
    type: 'page_id',
    title: 'Fanpage',
    page_id: '',
    condition: {}
  },
  {
    type: 'latest_conversation_time',
    title: 'Lần cuối nhắn tin'
  },
  {
    type: 'tags',
    title: 'Thẻ Tag',
    tags: '',
    condition: {}
  }
]
export const StatusSendMessage = [
  {
    key: 'plan',
    value: 'Đã lên lịch',
    color: '#1890FF'
  },
  {
    key: 'sent',
    value: 'Đã gửi ',
    color: '#48B911'
  },
  {
    key: 'mockup',
    value: 'Nháp',
    color: '#595959'
  }
]

export function handleFilter(data) {
  const dataQueryFilter = []
  const dataFilter = data.map((r) => {
    return {
      ...r,
      arrayFilterType: !r[`${r.type}`].key
        ? r[`${r.type}`].includes(',')
          ? r[`${r.type}`].split(',').map((r) => r.trim())
          : [r[`${r.type}`].trim()]
        : r[`${r.type}`].key
    }
  })
  dataFilter.forEach((r) => {
    if (r.condition && r.condition.key) {
      if (r.condition.key === 'start_with' || r.condition.key === 'end_with') {
        if (r.arrayFilterType.length > 1) {
          dataQueryFilter.push({
            or: r.arrayFilterType.map((t) => {
              return {
                name: r.type,
                op: r.condition.key,
                val: t
              }
            })
          })
        } else {
          dataQueryFilter.push({
            name: r.type,
            op: r.condition.key,
            val: r.arrayFilterType[0]
          })
        }
      }
      if (
        r.type !== 'start_with' &&
        r.type !== 'end_with' &&
        r.condition &&
        r.condition.key
      ) {
        if (
          r.condition.key === 'is_empty' ||
          r.condition.key === 'is_not_empty'
        ) {
          dataQueryFilter.push({
            name: r.type,
            op: r.condition.key,
            val: ''
          })
        }
        if (
          r.arrayFilterType.length > 1 &&
          r.condition.key !== 'is_empty' &&
          r.condition.key !== 'is_not_empty'
        ) {
          if (r.condition.key === 'like') {
            dataQueryFilter.push({
              name: r.type,
              op: 'in',
              val: r.arrayFilterType
            })
          } else if (r.condition.key === 'not_like') {
            dataQueryFilter.push({
              not: {
                name: r.type,
                op: 'in',
                val: r.arrayFilterType
              }
            })
          }
        }
        if (
          r.arrayFilterType.length <= 1 &&
          r.condition.key !== 'is_empty' &&
          r.condition.key !== 'is_not_empty'
        ) {
          if (r.condition.key === 'not_like') {
            dataQueryFilter.push({
              not: {
                name: r.type,
                op: 'like',
                val: r.arrayFilterType[0]
              }
            })
          } else if (r.condition.key === 'like') {
            dataQueryFilter.push({
              name: r.type,
              op: r.condition.key,
              val: r.arrayFilterType[0]
            })
          }
        }
      }
    }
    if (!r.condition || !r.condition.key) {
      if (r.type === 'latest_conversation_time') {
        if (
          r.latest_conversation_time.key.startDateMessage ===
          r.latest_conversation_time.key.endDateMessage
        ) {
          dataQueryFilter.push({
            name: r.type,
            op: 'geq',
            val:
              moment(
                r.latest_conversation_time.key.startDateMessage
              ).valueOf() / 1000
          })
        } else {
          dataQueryFilter.push({
            and: [
              {
                name: r.type,
                op: 'geq',
                val:
                  moment(
                    r.latest_conversation_time.key.startDateMessage
                  ).valueOf() / 1000
              },
              {
                name: r.type,
                op: 'leq',
                val:
                  moment(
                    r.latest_conversation_time.key.endDateMessage
                  ).valueOf() /
                    1000 +
                  86400
              }
            ]
          })
        }
      } else if (r.type === 'date_of_birth') {
        if (r.date_of_birth.key.startDate === r.date_of_birth.key.endDate) {
          dataQueryFilter.push({
            name: r.type,
            op: 'geq',
            val: moment(r.date_of_birth.key.startDate).valueOf() / 1000
          })
        } else {
          dataQueryFilter.push({
            and: [
              {
                name: r.type,
                op: 'geq',
                val: moment(r.date_of_birth.key.startDate).valueOf() / 1000
              },
              {
                name: r.type,
                op: 'leq',
                val:
                  moment(r.date_of_birth.key.endDate).valueOf() / 1000 + 86400
              }
            ]
          })
        }
      } else {
        dataQueryFilter.push({
          name: r.type,
          op: 'in',
          val: r.arrayFilterType
        })
      }
    }
  })
  return dataQueryFilter
}
export const StatusMessageSpam = [
  {
    name: 'Sent',
    key: 'done',
    color: '#48B911',
    index: 1,
    status: 'done',
    icon: 'svg/SendSuccess.svg'
  },
  {
    key: 'start',
    name: 'Đã lên lịch',
    color: '#1890FF',
    index: 2,
    status: 'start',
    icon: 'svg/Rolling.svg'
  },
  {
    name: 'Handling',
    key: 'processing',
    color: '#1890FF',
    index: 3,
    icon: 'svg/Rolling.svg'
  },

  {
    name: 'Draft',
    key: 'init',
    color: '#8C8C8C',
    index: 4,
    status: 'init'
  },
  {
    name: 'Have errors',
    key: 'error',
    color: '#F5222D',
    index: 5,
    status: 'error',
    icon: 'mdi-close-circle'
  }
]
export const StatusMessageJourney = [
  {
    name: 'Working',
    key: 'start',
    color: '#1890FF',
    index: 1,
    icon: 'svg/Rolling.svg',
    status: 'start'
  },
  {
    name: 'Stopping',
    key: 'pause',
    color: '#FFBE07',
    index: 2,
    status: 'pause',
    icon: 'mdi-pause'
  },
  {
    name: 'Draft',
    key: 'init',
    color: '#8C8C8C',
    index: 1,
    status: 'init'
  },
  {
    name: 'Have errors',
    key: 'error',
    color: '#F5222D',
    index: 5,
    status: 'error',
    icon: 'mdi-close-circle'
  }
]
export const StatusMessage = [
  {
    name: 'Draft',
    key: 'init',
    color: '#8C8C8C',
    index: 1,
    status: 'init'
  },
  {
    key: 'start',
    name: 'Processing',
    color: '#1890FF',
    index: 2,
    status: 'start',
    icon: 'svg/Rolling.svg'
  },
  {
    name: 'Sent',
    key: 'done',
    color: '#48B911',
    index: 3,
    status: 'done',
    icon: 'svg/SendSuccess.svg'
  },
  {
    name: 'Stop schedule',
    key: 'pause',
    color: '#F5222D',
    index: 4,
    status: 'pause',
    icon: 'mdi-pause'
  },
  {
    name: 'Have errors',
    key: 'error',
    color: '#F5222D',
    index: 5,
    status: 'error',
    icon: 'mdi-close-circle'
  },
  {
    name: 'Continue',
    key: 'continue',
    color: '#262626',
    index: 6,
    status: 'start',
    icon: 'mdi-menu-right '
  },
  {
    name: 'Processing',
    key: 'processing',
    color: '#1890FF',
    index: 7,
    icon: 'svg/Rolling.svg'
  },
  {
    name: 'Processing',
    key: 'lock',
    color: '#1890FF',
    index: 7,
    icon: 'svg/Rolling.svg'
  }
]

export const objSends = []

export function arrayMove(arr, oldIndex, newIndex) {
  if (newIndex >= arr.length) {
    let k = newIndex - arr.length + 1
    while (k--) {
      arr.push(undefined)
    }
  }
  arr.splice(newIndex, 0, arr.splice(oldIndex, 1)[0])
  return arr
}

export const actionTypesScenarios = [
  { text: 'Văn bản', value: 'message', icon: 'svg/actions/text.svg' },
  // { text: 'Time typing', value: 'time_typing', icon: 'svg/actions/clock.svg' },
  { text: 'Image', value: 'picture', icon: 'svg/actions/image.svg' },
  { text: 'Slider', value: 'carousel', icon: 'svg/actions/slider.svg' },
  { text: 'Video', value: 'video', icon: 'svg/actions/video.svg' },
  {
    text: 'Trả lời nhanh',
    value: 'quick_replies',
    icon: 'svg/actions/quick_reply.svg'
  },
  {
    text: 'Yêu cầu NV',
    value: 'connect_to_live_agent',
    icon: 'svg/actions/agent.svg'
  },
  {
    text: 'Thu thập thông tin',
    value: 'requiment',
    icon: 'svg/actions/library.svg'
  },
  {
    text: 'Đi đến Block',
    value: 'redirect_skill',
    icon: 'svg/actions/refresh.svg'
  },
  {
    text: 'Chỉnh sửa Memory',
    value: 'memory_edit',
    icon: 'svg/actions/edit.svg'
  },
  {
    text: 'Gán vào lộ trình',
    value: 'assign_to_journey',
    icon: 'svg/AssginJourney.svg'
  },
  {
    text: 'Dừng lộ trình',
    value: 'update_status_in_journey',
    icon: 'svg/journey_remove.svg'
  },
  {
    text: 'Tìm kiếm dữ liệu',
    value: 'db_search',
    icon: 'svg/actions/dbsearch.svg'
  }
  // { text: 'contact with staff', value: 'handover' }
]

export function getIndicesOf(searchStr, str, caseSensitive) {
  const searchStrLen = searchStr.length
  if (searchStrLen === 0) {
    return []
  }
  let startIndex = 0
  let index = ''
  const indices = []
  if (!caseSensitive) {
    str = str.toLowerCase()
    searchStr = searchStr.toLowerCase()
  }
  while ((index = str.indexOf(searchStr, startIndex)) > -1) {
    indices.push(index)
    startIndex = index + searchStrLen
  }
  return indices
}

const accentsMap = {
  a: 'à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ',
  e: 'è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ',
  i: 'ì|í|ị|ỉ|ĩ',
  o: 'ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ',
  u: 'ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ',
  y: 'ỳ|ý|ỵ|ỷ|ỹ',
  d: 'đ'
}

export const slugify = (text) =>
  Object.keys(accentsMap).reduce(
    (acc, cur) => acc.replace(new RegExp(accentsMap[cur], 'g'), cur),
    text
  )

export function extractHostname(url) {
  const match = url.match(/:\/\/(www[0-9]?\.)?(.[^/:]+)/i)
  if (
    match != null &&
    match.length > 2 &&
    typeof match[2] === 'string' &&
    match[2].length > 0
  ) {
    return match[2]
  } else {
    return null
  }
}

export const facebookAllowedFileTypeUploads = [
  'application/msword',
  'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
  'image/jpeg',
  'image/png',
  'image/gif',
  'image/svg+xml',
  'image/webp',
  'application/pdf',
  'text/csv',
  'application/vnd.ms-excel',
  'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
  'application/pdf',
  'application/vnd.ms-powerpoint',
  'application/vnd.openxmlformats-officedocument.presentationml.presentation',
  'audio/mpeg',
  'audio/mp3',
  'video/mpeg',
  'video/x-msvideo',
  'video/mp4',
  'text/plain'
]

export const zaloAllowedFileTypeUploads = [
  'application/msword',
  'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
  'image/jpeg',
  'image/png',
  'image/gif',
  'application/pdf'
]

export const ZALO_MAX_UPLOAD_SIZE = 5
export const FACEBOOK_MAX_UPLOAD_SIZE = 25

export const convertTimeToMinutes = (num, type) => {
  let time = 0
  if (type === 'hour') {
    time = num * 60
  } else if (type === 'day') {
    time = num * 720
  } else {
    time = num
  }
  return time
}

export const convertTexttoVietNamese = {
  'sys.email': 'Email',
  'sys.phone_number': 'Số điện thoại',
  'sys.number': 'Số',
  'sys.datetime': 'Ngày giờ',
  'sys.color': 'Màu sắc',
  'sys.money': 'Tiền',
  'sys.mass': 'Khối lượng',
  'sys.duration': 'Thời hạn',
  'sys.geo_province': 'Tỉnh/ thành phố',
  'sys.geo_district': 'Quận/huyện',
  'sys.geo_country': 'Quốc gia',
  'sys.geo_commune': 'Xã/phường/thị trấn',
  'sys.address': 'Địa chỉ',
  'sys.person': 'Tên người'
}

export const businessFields = [
  'Bán buôn và bán lẻ',
  'Thời trang & phụ kiện',
  'Mẹ và bé',
  'Gia dụng & Điện tử điện máy',
  'Ô tô xe máy',
  'Làm đẹp',
  'Dã ngoại & thể thao',
  'Nội thất & xây dựng',
  'Nhà hàng & F&B',
  'Du lịch & khách sạn',
  'Hoạt động truyền thông',
  'Tài chính ngân hàng',
  'Bất động sản',
  'Chuyên môn & lĩnh vực tư vấn',
  'Giáo dục',
  'Y tế',
  'Nghệ thuật & giải trí',
  'Khác'
]

export const companySizes = [
  '< 10 người',
  '10 - 50 người',
  '50 - 100 người',
  '100 - 200 người',
  '200 - 500 người',
  '> 500 người'
]
export const typeDataStore = [
  {
    text: 'Số',
    value: 'number'
  },
  {
    text: 'Văn bản',
    value: 'text'
  },
  {
    text: 'Ngày',
    value: 'date'
  },
  {
    text: 'Giờ',
    value: 'time'
  },
  {
    text: 'Ngày giờ',
    value: 'date_time'
  },
  {
    text: 'Một lựa chọn',
    value: 'single_choice'
  },
  {
    text: 'Nhiều lựa chọn',
    value: 'multiple_choice'
  }
]

export const ChangeToSlug = (str) => {
  str = str.toLowerCase()

  // xóa dấu
  str = str.replace(/(à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ)/g, 'a')
  str = str.replace(/(è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ)/g, 'e')
  str = str.replace(/(ì|í|ị|ỉ|ĩ)/g, 'i')
  str = str.replace(/(ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ)/g, 'o')
  str = str.replace(/(ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ)/g, 'u')
  str = str.replace(/(ỳ|ý|ỵ|ỷ|ỹ)/g, 'y')
  str = str.replace(/(đ)/g, 'd')

  // Xóa ký tự đặc biệt
  str = str.replace(/([^0-9a-z-\s])/g, '')

  // Xóa khoảng trắng thay bằng ký tự -
  str = str.replace(/(\s+)/g, '_')

  // xóa phần dự - ở đầu
  str = str.replace(/^-+/g, '')

  // xóa phần dư - ở cuối
  str = str.replace(/_+$/g, '')

  // return
  return str
}

export const formatWithCommas = (x) => {
  const parts = x.toString().split('.')
  parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ',')
  return parts.join('.')
}

export const defautlData = {
  store_name: '',
  fields: [
    {
      field_name: 'Tên',
      field_code: 'ten',
      field_type: 'text',
      payload: null
    },
    {
      field_name: 'Mô tả',
      field_code: 'mo_ta',
      field_type: 'text',
      payload: null
    },
    {
      field_name: 'Tồn kho',
      field_code: 'ton_kho',
      field_type: 'number',
      payload: null
    }
  ]
}

export const actions = [
  {
    text: 'Message',
    type: 'header'
  },
  {
    text: 'Message Template',
    img: 'message.svg',
    value: 'message_template'
  },
  {
    text: 'Text',
    img: 'message.svg',
    value: 'message'
  },
  {
    text: 'Quick reply',
    img: 'quick_replies.svg',
    value: 'quick_replies'
  },
  {
    text: 'Buttons',
    img: 'button.svg',
    value: 'buttons'
  },
  {
    text: 'Picture',
    img: 'image.svg',
    value: 'picture'
  },
  {
    text: 'Video',
    img: 'video.svg',
    value: 'video'
  },
  {
    text: 'Carousel',
    img: 'carousel.svg',
    value: 'carousel'
  },
  {
    text: 'Webhood',
    img: 'webhood.svg',
    value: 'webhook'
  },
  {
    text: 'Action',
    type: 'header'
  },
  {
    text: 'Redirect skill',
    img: 'redirect_skill.svg',
    value: 'redirect_skill'
  },
  {
    text: 'Edit memory',
    img: 'editmemory.svg',
    value: 'memory_edit'
  },
  {
    text: 'Connect to live agent',
    img: 'connecttolive.svg',
    value: 'connect_to_live_agent'
  },
  {
    text: 'API call',
    img: 'apicall.svg',
    value: 'api_call'
  },
  {
    text: 'DB search',
    img: 'dbsearch.svg',
    value: 'db_search'
  },
  {
    text: 'Save order',
    img: 'saveorder.svg',
    value: 'save_order'
  },
  {
    text: 'Save to cart',
    img: 'savetocard.svg',
    value: 'save_to_cart'
  },
  {
    text: 'Remove cart item',
    img: 'removecard.svg',
    value: 'remove_cart_item'
  },
  {
    text: 'Save customer info',
    img: 'savecustomeruser.svg',
    value: 'save_customer_info'
  },
  {
    text: 'Load customer info',
    img: 'loadcustomer.svg',
    value: 'load_customer_info'
  }
]
