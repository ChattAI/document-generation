/* eslint-disable unicorn/number-literal-case */
const cache = {}

// Generate a Hash for the String
const hash = (word) => {
  let h = 0
  for (let i = 0; i < word.length; i++) {
    h = word.charCodeAt(i) + ((h << 5) - h)
  }
  return h
}

// Change the darkness or lightness
const shade = (color, prc) => {
  const num = parseInt(color, 16)
  const amt = Math.round(2.55 * prc)
  const R = (num >> 16) + amt
  const G = ((num >> 8) & 0x00ff) + amt
  const B = (num & 0x0000ff) + amt
  return (
    0x1000000 +
    (R < 255 ? (R < 1 ? 0 : R) : 255) * 0x10000 +
    (G < 255 ? (G < 1 ? 0 : G) : 255) * 0x100 +
    (B < 255 ? (B < 1 ? 0 : B) : 255)
  )
    .toString(16)
    .slice(1)
}

// Convert init to an RGBA
const intToRGBA = (i) => {
  const color =
    ((i >> 24) & 0xff).toString(16) +
    ((i >> 16) & 0xff).toString(16) +
    ((i >> 8) & 0xff).toString(16) +
    (i & 0xff).toString(16)
  return color
}

const stringToColor = (str) => {
  return shade(intToRGBA(hash(str)), -10)
}

export const stringColor = (str) => {
  return (cache[str] = cache[str] || stringToColor(str))
}
