import Cookie from 'js-cookie'

export const setToken = (token, user) => {
  if (process.SERVER_BUILD) return
  window.localStorage.setItem('token', token)
  window.localStorage.setItem('user', JSON.stringify(user))
  Cookie.set('token', token)
}

export const unSetToken = () => {
  if (process.SERVER_BUILD) return
  window.localStorage.removeItem('token')
  window.localStorage.removeItem('user')
  Cookie.remove('token')
}

export const getToken = () => {
  if (process.SERVER_BUILD) return
  return Cookie.get('token')
}

export const getTokenFromCookie = (req) => {
  if (!req || !req.headers.cookie) return
  const tokenCookie = req.headers.cookie
    .split(';')
    .find((c) => c.trim().startsWith('token='))
  if (!tokenCookie) return
  const token = tokenCookie.split('=')[1]
  return token
}

export const getTokenFromLocalStorage = () => {
  const token = window.localStorage.token
  return token || ''
}

export const getUserFromLocalStorage = () => {
  const json = window.localStorage.user
  return json ? JSON.parse(json) : undefined
}
