/* eslint-disable camelcase */
// import * as sortBy from 'lodash.sortby'

export const splitWithOffsets = (text, offsets, isChild = false, poffset) => {
  let lastEnd = poffset ? poffset.start_offset : 0
  const splits = []
  // console.log('aaa', text, offsets, isChild, poffset)
  if (!offsets || (offsets.length === 0 && isChild)) return splits
  for (const offset of offsets) {
    const { start_offset, end_offset } = offset
    // console.log('bbb', lastEnd, start_offset, end_offset)
    if (lastEnd < start_offset) {
      const newItem = {
        start_offset: lastEnd,
        end_offset: start_offset,
        content: text.slice(lastEnd, start_offset)
      }
      // console.log('bbb----1', newItem, start_offset, end_offset)
      splits.push(newItem)
    }
    const newItem = {
      ...offset,
      token: true,
      content: text.slice(start_offset, end_offset),
      children:
        isChild ||
        offset.children === undefined ||
        (offset.children && offset.children.length === 0)
          ? []
          : splitWithOffsets(text, offset.children, true, {
              start_offset,
              end_offset
            })
    }
    // console.log('bbb----2', newItem, start_offset, end_offset)
    splits.push(newItem)
    lastEnd = end_offset
  }
  if (isChild && poffset) {
    if (lastEnd < poffset.end_offset) {
      splits.push({
        start_offset: lastEnd,
        end_offset: poffset.end_offset,
        content: text.slice(lastEnd, poffset.end_offset)
      })
    }
  } else if (lastEnd < text.length) {
    splits.push({
      start_offset: lastEnd,
      end_offset: text.length,
      content: text.slice(lastEnd, text.length)
    })
  }
  // console.log('splits', splits);
  return splits
}

export const selectionIsEmpty = (selection) => {
  return selection.focusOffset === selection.anchorOffset
}

export const selectionIsBackwards = (selection) => {
  if (selectionIsEmpty(selection)) return false

  const position = selection.anchorNode.compareDocumentPosition(
    selection.focusNode
  )

  let backward = false
  if (
    (!position && selection.anchorOffset > selection.focusOffset) ||
    position === Node.DOCUMENT_POSITION_PRECEDING
  )
    backward = true

  return backward
}
