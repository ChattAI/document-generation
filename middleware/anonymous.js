// import { getUserFromLocalStorage } from '~/utils/auth'

export default function({ store, redirect }) {
  if (store.getters.token && store.getters.token !== '') {
    return redirect('/')
  }
}
