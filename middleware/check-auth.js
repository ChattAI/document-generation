import {
  getTokenFromCookie,
  getTokenFromLocalStorage,
  getUserFromLocalStorage
} from '~/utils/auth'

export default function(context) {
  const { store, req } = context
  // If nuxt generate, pass this middleware
  if (process.server && !req) return
  const token = process.server
    ? getTokenFromCookie(req)
    : getTokenFromLocalStorage()

  const user = process.server ? undefined : getUserFromLocalStorage()
  if (!process.servertoken !== '' && store.getters.token === '') {
    store.commit('SET_TOKEN', token)
  }
  if (!process.server && user && store.getters.user === undefined) {
    store.commit('SET_USER_INFO', user)
  }
}
