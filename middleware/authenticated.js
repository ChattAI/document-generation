export default function({ app, store, redirect }) {
  if (store.getters.token === undefined || store.getters.token === '') {
    const path = app.localePath('login')
    return redirect(path)
  }
}
