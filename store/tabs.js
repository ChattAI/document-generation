export const state = () => ({
  tabAction: 0,
  tabAutoComment: 0
})

export const mutations = {
  SET_TAB_ACTION: (state, tabAction) => {
    state.tabAction = tabAction
  },
  SET_TAB_AUTOCOMMENT: (state, action) => {
    state.tabAutoComment = action
  },
  RESET_TAB: (state) => {
    state.tabAutoComment = 0
  }
}
