export const state = () => ({
  botID: null,
  conversations: [],
  conversationCurrentPage: 0,
  allConversationsLoaded: false,
  channels: [],
  customerTags: [],
  checkedChannels: null,
  filters: {
    q: '',
    channel_ids: null,
    tags: [],
    supporters: [],
    has_phone: null,
    has_new_message: null,
    is_handover: null,
    need_support: null,
    bot_unknown: null,
    startDate: null,
    endDate: null,
    client_type: 'all'
  },
  supporters: []
})

export const mutations = {
  SET_CONVERSATIONS: (state, data) => {
    state.conversations = data
  },
  ADD_CONVERSATION: (state, data) => {
    const existItem = state.conversations.find((item) => item.id === data.id)
    if (!existItem) {
      state.conversations.unshift(data)
    }
  },
  ADD_CONVERSATION_SORT: (state, data) => {
    const index = state.conversations.findIndex((item) => {
      const time = item.last_received || item.created_time
      return time < (data.last_received || data.created_time)
    })
    state.conversations.splice(index, 0, data)
  },
  ADD_CONVERSATIONS: (state, arr) => {
    arr.forEach((data) => {
      const existItem = state.conversations.find((item) => item.id === data.id)
      if (!existItem) {
        state.conversations.push(data)
      }
    })
  },
  UPDATE_CONVERSATION: (state, { data, type }) => {
    // type in [last_message, tags, has_unread_message]
    const index = state.conversations.findIndex((item) => item.id === data.id)
    if (index !== -1) {
      switch (type) {
        case 'last_message':
          state.conversations.unshift(
            Object.assign({}, data, { tags: state.conversations[index].tags })
          )
          state.conversations.splice(index + 1, 1)
          break
        case 'tags':
          state.conversations.splice(index, 1, data)
          break
        default:
          state.conversations.splice(index, 1, data)
          break
      }
    }
  },
  DELETE_CONVERSATION: (state, data) => {
    const index = state.conversations.findIndex((item) => item.id === data.id)
    if (index !== -1) {
      state.conversations.splice(index, 1)
    }
  },
  SET_CONVERSATION_PAGE: (state, page) => {
    state.conversationCurrentPage = page
  },
  SET_ALL_CONVERSATIONS_LOADED: (state, value) => {
    state.allConversationsLoaded = value
  },
  SET_CHANNELS: (state, data) => {
    state.channels = data
  },
  SET_CUSTOMER_TAGS: (state, data) => {
    state.customerTags = data
  },
  ADD_CUSTOMER_TAG: (state, data) => {
    state.customerTags.push(data)
  },
  UPDATE_CUSTOMER_TAG: (state, data) => {
    const index = state.customerTags.findIndex((item) => item.id === data.id)
    if (index !== -1) {
      state.customerTags.splice(index, 1, data)
    }
  },
  REMOVE_CUSTOMER_TAG: (state, tagID) => {
    const index = state.customerTags.findIndex((item) => item.id === tagID)
    if (index !== -1) {
      const tagName = state.customerTags[index].name
      state.customerTags.splice(index, 1)
      state.conversations.forEach((item) => {
        if (item.tags && item.tags.includes(tagName)) {
          item.tags.splice(item.tags.indexOf(tagName), 1)
        }
      })
    }
  },
  SET_BOT_ID: (state, botID) => {
    state.botID = botID
  },
  SET_CHECKED_CHANNELS: (state, data) => {
    state.checkedChannels = data
  },
  SET_FILTERS: (state, filterObject) => {
    state.filters = Object.assign({}, filterObject)
  },
  SET_SUPPORTERS: (state, data) => {
    state.supporters = data
  }
}

export const actions = {
  setBotID({ commit }, botID) {
    commit('SET_BOT_ID', botID)
  },
  setConversations({ commit }, data) {
    commit('SET_CONVERSATIONS', data)
  },
  addConversation({ commit }, data) {
    commit('ADD_CONVERSATION', data)
  },
  updateConversation({ commit }, object) {
    const updateParams = {}
    // if (object.type === 'tags') {
    //   updateParams['tags'] = object.data.tags
    // }
    if (object.type === 'supporter_id') {
      // eslint-disable-next-line dot-notation
      updateParams['supporter_id'] = object.data.supporter_id
    }
    if (Object.keys(updateParams).length) {
      this.$axios.put(
        `/api_conversation/conversation/bots/${object.data.bot_id}/channels/${object.data.channel_id}/clients/${object.data.id}/`,
        updateParams
      )
    }
    commit('UPDATE_CONVERSATION', object)
  },
  deleteConversation({ commit }, data) {
    return new Promise((resolve, reject) => {
      this.$axios
        .delete(
          `/api_conversation/conversation/bots/${data.bot_id}/channels/${data.channel_id}/clients/${data.id}/`
        )
        .then((response) => {
          if (response.error_code === 0) {
            commit('DELETE_CONVERSATION', data)
            resolve(response)
          } else {
            reject(response.message)
          }
        })
        .catch((error) => {
          reject(error)
        })
    })
  },
  fetchConversations({ commit }, { botID, params }) {
    return new Promise((resolve, reject) => {
      this.$axios
        .get(`/api_conversation/conversation/bots/${botID}/conversations`, {
          params: { ...params, size: 15 }
        })
        .then((response) => {
          if (response.error_code === 0) {
            commit('SET_CONVERSATIONS', response.data.items)
            commit('SET_ALL_CONVERSATIONS_LOADED', false)
            commit('SET_CONVERSATION_PAGE', 0)
            resolve(response.data.items)
          } else {
            reject(response.message)
          }
        })
        .catch((error) => {
          reject(error)
        })
    })
  },
  fetchMoreConversations({ commit, state }, { botID, params }) {
    return new Promise((resolve, reject) => {
      this.$axios
        .get(`/api_conversation/conversation/bots/${botID}/conversations`, {
          params: {
            ...params,
            size: 15,
            page: state.conversationCurrentPage + 1
          }
        })
        .then((response) => {
          if (response.error_code === 0) {
            if (response.data.items.length) {
              commit('ADD_CONVERSATIONS', response.data.items)
              commit('SET_CONVERSATION_PAGE', state.conversationCurrentPage + 1)
            } else {
              commit('SET_ALL_CONVERSATIONS_LOADED', true)
            }
            resolve(response.data.items)
          } else {
            reject(response.message)
          }
        })
        .catch((error) => {
          reject(error)
        })
    })
  },
  fetchChannels({ commit }, { botID }) {
    return new Promise((resolve, reject) => {
      this.$axios
        .get(`/api/bots/${botID}/channels/`)
        .then((response) => {
          if (response.code === 200) {
            commit('SET_CHANNELS', response.data.items)
            resolve(response.data)
          } else {
            reject(response.message)
          }
        })
        .catch((error) => {
          reject(error)
        })
    })
  },
  fetchCustomerTags({ commit }, { botID }) {
    return new Promise((resolve, reject) => {
      this.$axios
        .get(`/api_crm/bots/${botID}/customer-tags/`)
        .then((response) => {
          if (response.code === 0) {
            commit('SET_CUSTOMER_TAGS', response.data.items)
            resolve(response.data)
          } else {
            reject(response.message)
          }
        })
        .catch((error) => {
          reject(error)
        })
    })
  },
  createCustomerTag({ commit }, { botID, data }) {
    return new Promise((resolve, reject) => {
      this.$axios
        .post(`/api_crm/bots/${botID}/customer-tags/`, data)
        .then((response) => {
          if (response.code === 0) {
            commit('ADD_CUSTOMER_TAG', response.data)
            resolve(response.data)
          } else {
            reject(response.message)
          }
        })
        .catch((error) => {
          reject(error)
        })
    })
  },
  updateCustomerTag({ commit }, { botID, tagID, data }) {
    return new Promise((resolve, reject) => {
      this.$axios
        .put(`/api_crm/bots/${botID}/customer-tags/${tagID}`, data)
        .then((response) => {
          if (response.code === 0) {
            commit('UPDATE_CUSTOMER_TAG', response.data)
            resolve(response.data)
          } else {
            reject(response.message)
          }
        })
        .catch((error) => {
          reject(error)
        })
    })
  },
  deleteCustomerTag({ commit }, { botID, tagID }) {
    return new Promise((resolve, reject) => {
      this.$axios
        .delete(`/api_crm/bots/${botID}/customer-tags/${tagID}`)
        .then((response) => {
          if (response.code === 0) {
            commit('REMOVE_CUSTOMER_TAG', tagID)
            resolve(1)
          } else {
            reject(response.message)
          }
        })
        .catch((error) => {
          reject(error)
        })
    })
  },
  setCheckedChannels({ commit }, data) {
    commit('SET_CHECKED_CHANNELS', data)
  },
  setFilters({ commit }, data) {
    commit('SET_FILTERS', data)
  },
  fetchSupporters({ commit }, { botID }) {
    return new Promise((resolve, reject) => {
      this.$axios
        .get(`/api_authorization/bots/${botID}/members`, {
          params: { active: true }
        })
        .then((response) => {
          if (response.code === 0) {
            commit('SET_SUPPORTERS', response.data.items)
            resolve(response.data)
          } else {
            reject(response.message)
          }
        })
        .catch((error) => {
          reject(error)
        })
    })
  },
  fetchConversationByUserId({ commit }, { botID, channelID, userID }) {
    return new Promise((resolve, reject) => {
      this.$axios
        .get(`/api_conversation/conversation/bots/${botID}/conversations`, {
          params: {
            channel_ids: [channelID],
            user_id: userID,
            client_type: 'message'
          }
        })
        .then((response) => {
          if (
            response.error_code === 0 &&
            response.data.items &&
            response.data.items.length
          ) {
            commit('ADD_CONVERSATION_SORT', response.data.items[0])
            resolve(response.data.items[0])
          } else {
            reject(Error('Không tìm thấy dữ liệu'))
          }
        })
        .catch((error) => {
          reject(error)
        })
    })
  },
  // only use to refetch for missing data client
  refetchClient({ commit }, { botID, channelID, clientID }) {
    return new Promise((resolve, reject) => {
      this.$axios
        .get(
          `/api_conversation/conversation/bots/${botID}/channels/${channelID}/clients/${clientID}/`
        )
        .then((response) => {
          if (response.error_code === 0 && response.data) {
            commit('UPDATE_CONVERSATION', { data: response.data, type: 'all' })
            resolve(response.data)
          } else {
            reject(Error('Không tìm thấy dữ liệu'))
          }
        })
        .catch((error) => {
          reject(error)
        })
    })
  }
}

export const getters = {
  conversations(state) {
    return state.conversations
  },
  channels(state) {
    return state.channels
  },
  customerTags(state) {
    return state.customerTags
  },
  botID(state) {
    return state.botID
  },
  checkedChannels(state) {
    return state.checkedChannels
  },
  allConversationsLoaded(state) {
    return state.allConversationsLoaded
  },
  filters(state) {
    return state.filters
  },
  supporters(state) {
    return state.supporters
  }
}
