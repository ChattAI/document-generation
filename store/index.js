/* eslint-disable no-console */
// import jwt from 'jsonwebtoken'
import { setToken, unSetToken, getTokenFromCookie } from '~/utils/auth'

export const state = () => ({
  token: '',
  user: undefined,
  showTestChat: false,
  showTestCaseChat: false,
  dataTestFail: [],
  customerId: '1|FACEBOOK_FACEBOOK_imol|facebook|1|3221446141237236|11_5_7',
  isExplainMenu: false,
  fieldStores: [],
  scenariosId: '',
  collectionId: [],
  typeDataDbsearch: 'message',
  slotNameDbSearch: []
})

export const mutations = {
  SET_TOKEN: (state, token) => {
    state.token = token
  },
  SET_FIELD_STORES: (state, fields) => {
    state.fieldStores = fields
  },
  SET_SCENARIOID: (state, id) => {
    state.scenariosId = id
  },
  SET_USER_INFO: (state, user) => {
    state.user = user
  },
  SET_CUSTOMER_INFO: (state, customerId) => {
    state.customerId = customerId
  },
  SET_EXPLAIN: (state, data) => {
    state.isExplainMenu = data
  },
  TOGGLE_TEST: (state) => {
    state.showTestChat = !state.showTestChat
  },
  TURN_OFF_TEST_CASE: (state) => {
    state.showTestCaseChat = !state.showTestCaseChat
  },
  TOGGLE_TEST_CASE: (state, data) => {
    state.showTestCaseChat = !state.showTestCaseChat
    state.dataTestFail = data
  },
  UPDATE_SLOT_NAME: (state, data) => {
    const slotIndex = state.slotNameDbSearch.findIndex((r) => r.id === data.id)
    state.slotNameDbSearch[slotIndex].slotname = data.slotname
  },
  UPDATE_SLOT_NAME_SKILL: (state, data) => {
    const slotIndex = state.slotNameDbSearch.findIndex((r) => r.id === data.id)
    if (
      state.slotNameDbSearch[slotIndex] &&
      state.slotNameDbSearch[slotIndex].action
    ) {
      state.slotNameDbSearch[slotIndex].action = []
      state.slotNameDbSearch[slotIndex].action = data.slotname
    }
  },
  SET_SLOT_NAME_DB_SEARCH: (state, data) => {
    state.slotNameDbSearch.push(data)
  },
  REMOVE_INDEX_DBSEARCH: (state, index) => {
    state.collectionId = state.collectionId.filter((r) => r.index !== index)
  },
  SET_TYPE_DATA_DB_SEARCH: (state, data) => {
    state.typeDataDbsearch = data
  },
  UPDATE_COLLECTION_ID: (state, data) => {
    // check exsit id
    const findIndex = state.collectionId.findIndex(
      (r) => r.index === data.index
    )
    if (findIndex > -1) {
      state.collectionId[findIndex] = data
    } else {
      state.collectionId.push(data)
    }
  },
  RESET_SESSION: (state) => {
    state.token = ''
    state.user = undefined
    state.showTestChat = false
    state.showTestCaseChat = false
    state.dataTestFail = {}
    state.isExplainMenu = false
    state.collectionId = []
    state.typeDataDbsearch = 'message'
    state.slotNameDbSearch = []
    unSetToken()
  }
}

export const actions = {
  // init from server side
  nuxtServerInit({ commit }, { req }) {
    const token = getTokenFromCookie(req)
    console.log('nuxtServerInit | token', token)
    if (token) {
      try {
        // const decoded = jwt.verify(token, 'vinbdi_precious_secret_key')
        // console.log('nuxtServerInit | decoded', decoded)
        commit('SET_TOKEN', token)
        // commit('SET_USER_INFO', {
        //   id: decoded.sub,
        //   display_name: decoded.name
        // })
      } catch (err) {
        console.log('nuxtServerInit | err', err)
      }
    }
  },
  // /*
  //  *  Get user info
  //  */
  // getUserInfo({ commit }, token) {
  //   return new Promise((resolve, reject) => {
  //     this.$axios({
  //       url: '/login/',
  //       method: 'post',
  //       params: { ...authInfo }
  //     })
  //       .then((response) => {
  //         console.log('login response', response)
  //         if (response.code === 200) {
  //           const { token, user } = response.data
  //           commit('SET_TOKEN', token)
  //           commit('SET_USER_INFO', user)
  //           setToken(token, user)
  //           resolve(user)
  //         } else {
  //           reject(response.msg)
  //         }
  //       })
  //       .catch((err) => {
  //         reject(err)
  //       })
  //   })
  // },
  /*
   *  Login by username and password
   */
  resetSession({ commit }) {
    commit('RESET_SESSION')
  },
  login({ commit }, authInfo) {
    return new Promise((resolve, reject) => {
      this.$axios({
        url: '/api/login/',
        method: 'post',
        data: { ...authInfo }
      })
        .then((response) => {
          console.log('login response', response)
          if (response.code === 200) {
            const { token, user } = response.data
            commit('SET_TOKEN', token)
            commit('SET_USER_INFO', user)
            setToken(token, user)
            resolve(user)
          } else {
            reject(response.msg)
          }
        })
        .catch((err) => {
          reject(err)
        })
    })
  },
  /*
   *  register new user by username and password
   */
  register({ commit }, authInfo) {
    return new Promise((resolve, reject) => {
      this.$axios({
        url: '/api/register',
        method: 'post',
        data: authInfo
      })
        .then((response) => {
          if (response.errCode === 0) {
            const { token, user } = response.data
            commit('SET_TOKEN', token)
            commit('SET_USER_INFO', user)
            setToken(token, user)
            resolve(user)
          } else {
            reject(response.msg)
          }
        })
        .catch((err) => {
          reject(err)
        })
    })
  },
  /*
   *  logout
   */
  logout({ commit }, authInfo) {
    return new Promise((resolve, reject) => {
      this.$axios({
        url: '/api/logout',
        method: 'delete'
      })
        .then((response) => {
          if (response.errCode === 0) {
            commit('SET_TOKEN', 'token')
            commit('SET_USER_INFO', undefined)
            unSetToken()
            resolve()
          } else {
            reject(response.msg)
          }
        })
        .catch(() => {
          commit('SET_TOKEN', 'token')
          commit('SET_USER_INFO', undefined)
          unSetToken()
          resolve()
        })
    })
  },
  toggleTest({ commit }) {
    commit('TOGGLE_TEST')
  },
  toggleTestCase({ commit }, data) {
    commit('TOGGLE_TEST_CASE', data)
  },
  turnoffTestCase({ commit }) {
    commit('TURN_OFF_TEST_CASE')
  }
}
