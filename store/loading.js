export const state = () => ({
  overlay: false,
  autoCommentActice: -1,
  perPageCustomer: 20,
  perPageSegment: 20,
  isLoadImage: false
})

export const mutations = {
  SET_LOADING: (state, status) => {
    state.overlay = status
  },
  SET_LOAD_IMAGE: (state, status) => {
    state.isLoadImage = status
  },
  SET_AUTO_COMMENT_ACTIVE: (state, id) => {
    state.autoCommentActice = id
  },
  SET_PAGE_CUSTOMER: (state, status) => {
    state.perPageCustomer = status
  },
  SET_PAGE_SEGMENT: (state, status) => {
    state.perPageSegment = status
  }
}
