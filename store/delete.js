export const state = () => ({
  loadingMessage: false,
  loadingPicture: false,
  loadingRedirect: false,
  loadingHuman: false,
  loadingVideo: false,
  loadingQuickReplies: false,
  loadingMemory: false,
  loadingDocument: false,
  loadingCarousel: false,
  loadingApiCall: false,
  loadingSaveOrder: false,
  loadingRemoveCartItem: false,
  loadingLoadCusInfo: false,
  loadingSaveCusInfo: false,
  loadingDbSearch: false
})

export const mutations = {
  SET_LOADING_MESSAGE: (state, status) => {
    state.loadingMessage = status
  },
  SET_LOADING_PICTURE: (state, status) => {
    state.loadingPicture = status
  },
  SET_LOADING_REDIRECT: (state, status) => {
    state.loadingRedirect = status
  },
  SET_LOADING_HUMAN: (state, status) => {
    state.loadingHuman = status
  },
  SET_LOADING_VIDEO: (state, status) => {
    state.loadingVideo = status
  },
  SET_LOADING_QUICK_REPLIES: (state, status) => {
    state.loadingQuickReplies = status
  },
  SET_LOADING_MEMORY: (state, status) => {
    state.loadingMemory = status
  },
  SET_LOADING_DOCUMENT: (state, status) => {
    state.loadingDocument = status
  },
  SET_LOADING_CAROUSEL: (state, status) => {
    state.loadingCarousel = status
  },
  SET_LOADING_APICALL: (state, status) => {
    state.loadingApiCall = status
  },
  SET_LOADING_SAVEORDER: (state, status) => {
    state.loadingSaveOrder = status
  },
  SET_LOADING_REMOVECARTITEM: (state, status) => {
    state.loadingRemoveCartItem = status
  },
  SET_LOADING_LOADCUSINFO: (state, status) => {
    state.loadingLoadCusInfo = status
  },
  SET_LOADING_SAVECUSINFO: (state, status) => {
    state.loadingSaveCusInfo = status
  },
  SET_LOADING_DBSEARCH: (state, status) => {
    state.loadingDbSearch = status
  }
}
