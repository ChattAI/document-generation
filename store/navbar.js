const initState = () => {
  return {
    backButton: {
      text: '',
      path: '',
      showSavingStatus: false
    },
    saving: false
  }
}

export const state = () => ({
  backButton: initState().backButton,
  saving: initState().saving
})

export const mutations = {
  SHOW_BUTTON: (state, { text, path, showSavingStatus }) => {
    state.backButton.text = text
    state.backButton.path = path
    state.backButton.showSavingStatus = showSavingStatus
  },
  HIDE_BUTTON: (state) => {
    state.backButton = initState().backButton
  },
  SHOW_SAVING: (state) => {
    state.saving = true
  },
  HIDE_SAVING: (state) => {
    state.saving = initState().saving
  }
}

export const actions = {
  showButton({ commit }, data) {
    commit('SHOW_BUTTON', data)
  },
  hideButton({ commit }) {
    commit('HIDE_BUTTON')
  }
}

export const getters = {
  backButton(state) {
    return state.backButton
  },
  saving(state) {
    return state.saving
  }
}
