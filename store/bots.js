/* eslint-disable no-console */
export const state = () => ({
  allBots: [],
  currentBot: undefined,
  intents: [],
  entities: [],
  getIntents: false,
  getEntities: false,
  skills: [],
  getSkills: false,
  channels: [],
  getChannels: false,
  faqs: [],
  overlay: true,
  submenuActive: 'home',
  faq: {},
  sourceIds: [],
  getAllScenarios: [],
  system_entites: [],
  getAllSkillGroups: [],
  stableCustomers: {},
  channelPosts: [],
  stores: [],
  spamJourneys: [],
  history: { total: 0, items: {} },
  testCases: []
})

export const getters = {
  allBots(state) {
    return state.allBots
  },
  selectedChannelPost(state) {
    return state.selectedChannelPost
  },
  spamJourneys(state) {
    return state.spamJourneys
  }
}

export const mutations = {
  SET_SPAM_JOURNEYS: (state, data) => {
    state.spamJourneys = data
  },
  SET_STABLE_CUSTOMERS: (state, data) => {
    state.stableCustomers = data
  },
  SET_SOURCE: (state, source) => {
    state.sourceIds = source
  },
  DELETED_BOT: (state, botID) => {
    const botIndex = state.allBots.findIndex((item) => item.id === botID)
    if (botIndex !== -1) {
      state.allBots.splice(botIndex, 1)
    }
  },
  UPDATED_BOT: (state, bot) => {
    const botIndex = state.allBots.findIndex((item) => item.id === bot.id)
    if (botIndex !== -1) {
      state.allBots.splice(botIndex, 1, bot)
    }
  },
  SET_BOTS: (state, bots) => {
    state.allBots = bots
  },
  SET_FAQS: (state, faqs) => {
    state.faqs = faqs
  },
  SET_CURRENT_BOT: (state, bot) => {
    state.currentBot = bot
  },
  CREATED_BOT: (state, newBot) => {
    state.allBots.push(newBot)
  },
  SET_INTENTS: (state, intents) => {
    state.intents = intents
    state.getIntents = true
  },
  CREATED_INTENT: (state, newIntent) => {
    // state.intents.unshift(newIntent)
    state.intents.push(newIntent)
  },
  UPDATED_INTENT: (state, newIntent) => {
    state.intents = state.intents.map((item) => {
      if (item.id === newIntent.id) {
        return { ...item, ...newIntent }
      }
      return item
    })
  },
  SET_SCENARIOS: (state, data) => {
    state.getAllScenarios = data
  },
  SET_STORE: (state, dataStore) => {
    state.stores = dataStore.list_store_metadata
  },
  CREATE_SCENARIOS: (state, data) => {
    state.getAllScenarios.push(data)
  },
  SET_SKILL_GROUP: (state, skillgroup) => {
    state.getAllSkillGroups = skillgroup
  },
  DELETED_INTENT: (state, intentID) => {
    state.intents = state.intents.filter((item) => item.id !== intentID)
  },
  SET_ENTITIES: (state, entities) => {
    state.entities = entities
    state.getEntities = true
  },
  CREATED_ENTITY: (state, newEntity) => {
    // state.entities.unshift(newEntity)
    state.entities.push(newEntity)
  },
  UPDATED_ENTITY: (state, newEntity) => {
    state.entities = state.entities.map((item) => {
      if (item.id === newEntity.id) {
        return { ...item, ...newEntity }
      }
      return item
    })
  },
  SET_SYSTEM_ENTITY: (state, sysetity) => {
    state.system_entites = sysetity
  },
  DELETED_ENTITY: (state, entityID) => {
    state.entities = state.entities.filter((item) => item.id !== entityID)
  },
  SET_SKILLS: (state, skills) => {
    state.skills = skills
    state.getSkills = true
  },
  CREATED_SKILL: (state, newSkill) => {
    state.skills.push(newSkill)
  },
  UPDATED_SKILL: (state, newSkill) => {
    state.skills = state.skills.map((item) => {
      if (item.id === newSkill.id) {
        return { ...item, ...newSkill }
      }
      return item
    })
  },
  DELETED_SKILL: (state, skillID) => {
    state.skills = state.skills.filter((item) => item.id !== skillID)
  },
  SET_CHANNELS: (state, channels) => {
    state.channels = channels.map((item) => {
      item.connected = true
      return item
    })
    state.getChannels = true
  },
  PREFETCH_CHANNEL: (state, newChannels) => {
    console.log('prefetch')
    for (let i = 0; i < newChannels.length; i++) {
      let flag = false
      // if an exist channel got new token, update it
      for (let j = 0; j < state.channels.length; j++) {
        if (state.channels[j].id === newChannels[i].id) {
          newChannels[i].connected = true
          state.channels[j] = newChannels[i]
          flag = true
          break
        }
      }
      // if a channel is not exist before, then push to channel list
      if (!flag) {
        newChannels[i].connected = false
        state.channels.push(newChannels[i])
      }
    }
  },
  CREATED_CHANNEL: (state, newChannel) => {
    newChannel.connected = true
    state.channels = state.channels.map((item) => {
      if (
        item.page_id === newChannel.page_id &&
        item.type === newChannel.type
      ) {
        return { ...item, ...newChannel }
      }
      return item
    })
  },
  ACTIVE_MUTATION_REDIRECT: (state, status) => {
    state.submenuActive = status
  },
  DELETED_CHANNEL: (state, channelID) => {
    state.channels = state.channels.map((item) => {
      if (item.id === channelID) {
        item.connected = false
      }
      return item
    })
  },
  CREATED_FAQ: (state, newFaq) => {
    state.faqs.push(newFaq)
  },
  GET_FAQ: (state, faq) => {
    state.faq = faq
  },
  UPDATED_FAQ: (state, newFaq) => {
    state.faqs = state.faqs.map((item) => {
      if (item.id === newFaq.id) {
        return { ...item, ...newFaq }
      }
      return item
    })
  },
  SET_LOADING: (state, status) => {
    state.overlay = status
  },
  DELETED_FAQ: (state, faqID) => {
    state.faqs = state.faqs.filter((item) => item.id !== faqID)
  },
  SET_CHANNELS_POSTS: (state, channels) => {
    state.channelPosts = channels
  },
  SET_HISTORY: (state, history) => {
    state.history.total = history.total
    state.history.items[history.page] = history.items
    state.history.items = JSON.parse(JSON.stringify(state.history.items))
  },
  SET_TEST_CASES: (state, testCases) => {
    state.testCases = testCases
  },
  RESET_STATE: (state) => {
    state.allBots = []
    state.currentBot = undefined
    state.intents = []
    state.entities = []
    state.getIntents = false
    state.getEntities = false
    state.skills = []
    state.getSkills = false
    state.channels = []
    state.getChannels = false
    state.faqs = []
    state.faq = {}
    state.overlay = true
    state.getAllScenarios = []
    state.getAllSkillGroups = []
    state.channelPosts = []
    state.stores = []
    state.history = { total: 0, items: {} }
    state.testCases = []
  }
}

export const actions = {
  updateLoading({ commit }, status) {
    commit('SET_LOADING', status)
  },
  resetState({ commit }) {
    commit('RESET_STATE')
  },
  setCurrentBot({ commit }, bot) {
    commit('SET_CURRENT_BOT', bot)
  },
  updateBot({ commit }, bot) {
    return new Promise((resolve, reject) => {
      this.$axios({
        url: `/api/bots/${bot.id}/`,
        method: 'patch',
        data: bot
      }).then((response) => {
        console.log('Bot updated', response)
        if (response.code === 200) {
          commit('UPDATED_BOT', response.data ? response.data : bot)
          resolve(response.data)
        } else {
          reject(response.message)
        }
      })
    })
  },
  deleteBot({ commit }, bot) {
    commit('UPDATED_BOT', Object.assign(bot, { processing: true }))
    return new Promise((resolve, reject) => {
      this.$axios({
        url: `/api/bots/${bot.id}/`,
        method: 'delete'
      })
        .then((response) => {
          console.log('delete bot => response', response)
          if (response.code === 200) {
            commit('DELETED_BOT', bot.id)
            resolve(response)
          } else {
            reject(response.message)
          }
        })
        .catch((error) => {
          reject(error)
        })
    })
  },
  getBotByID({ commit }, botID) {
    commit('RESET_STATE')
    return new Promise((resolve, reject) => {
      this.$axios({
        url: `/api/bots/${botID}/`,
        method: 'get'
      })
        .then((response) => {
          console.log('getBotByID => response', response)
          if (response.code === 200) {
            commit('SET_CURRENT_BOT', response.data)
            resolve(response.data)
          } else {
            reject(response.message)
          }
        })
        .catch((error) => {
          reject(error)
        })
    })
  },
  getAllBots({ commit }) {
    return new Promise((resolve, reject) => {
      this.$axios({
        url: '/api/bots/',
        method: 'get'
      })
        .then((response) => {
          console.log('getAllBots => response', response)
          if (response.code === 200) {
            commit('SET_BOTS', response.data.items)
            commit('SET_LOADING', false)
            resolve(response.data)
          } else {
            reject(response.message)
          }
        })
        .catch((error) => {
          reject(error)
        })
    })
  },
  createBot({ commit }, newBot) {
    return new Promise((resolve, reject) => {
      this.$axios({
        url: '/api/bots/',
        method: 'post',
        data: newBot
      }).then((response) => {
        console.log('getAllBots => response', response)
        if (response.code === 200) {
          commit('CREATED_BOT', response.data)
          resolve(response.data)
        } else {
          reject(response.message)
        }
      })
    })
  },
  train({ commit }, opts) {
    const { botID, type } = opts
    let trainMode = 0
    if (type === 'Entity') {
      trainMode = 2
    }
    if (type === 'Intent') {
      trainMode = 1
    }
    return new Promise((resolve, reject) => {
      this.$axios({
        url: `/api/bots/${botID}/train/retrain/`,
        method: 'post',
        data: {
          train_mode: trainMode
        }
      }).then((response) => {
        console.log('train => response', response)
        if (response.code === 200) {
          resolve(response.data)
        } else {
          reject(response.message)
        }
      })
    })
  },
  testChat({ commit }, opts) {
    const { botID, utterance } = opts
    return new Promise((resolve, reject) => {
      this.$axios({
        url: `/api/bots/${botID}/train/chat/`,
        method: 'post',
        data: {
          utterance
        }
      }).then((response) => {
        console.log('testChat => response', response)
        if (response.code === 0) {
          resolve({
            data: response.data,
            context: response.context,
            utterance
          })
        } else {
          reject(response.message)
        }
      })
    })
  },
  testChatCase({ commit }, opts) {
    const { botID, utterance, customerId } = opts
    return new Promise((resolve, reject) => {
      this.$axios({
        url: `/api/bots/${botID}/train/chat/`,
        method: 'post',
        data: {
          utterance,
          customer_id: customerId
        }
      }).then((response) => {
        console.log('testChat => response', response)
        if (response.code === 0) {
          resolve({
            data: response.data,
            context: response.context,
            utterance
          })
        } else {
          reject(response.message)
        }
      })
    })
  },
  getIntents({ commit }, botID) {
    return new Promise((resolve, reject) => {
      this.$axios({
        url: `/api/bots/${botID}/intents/`,
        method: 'get'
      })
        .then((response) => {
          console.log('getIntents => response', response)
          if (response.code === 200) {
            commit('SET_INTENTS', response.data.items)
            resolve(response.data)
          } else {
            reject(response.message)
          }
        })
        .catch((error) => {
          reject(error)
        })
    })
  },
  createIntent({ commit }, opts) {
    const { botID, newIntent } = opts
    return new Promise((resolve, reject) => {
      this.$axios({
        url: `/api/bots/${botID}/intents/`,
        method: 'post',
        data: newIntent
      })
        .then((response) => {
          console.log('createIntent => response', response)
          if (response.code === 200) {
            commit('CREATED_INTENT', response.data)
            resolve(response.data)
          } else {
            reject(response.message)
          }
        })
        .catch((err) => reject(err))
    })
  },
  updateIntent({ commit }, opts) {
    const { botID, newIntent } = opts
    return new Promise((resolve, reject) => {
      this.$axios({
        url: `/api/bots/${botID}/intents/${newIntent.id}/`,
        method: 'patch',
        data: newIntent
      })
        .then((response) => {
          console.log('createIntent => response', response)
          if (response.code === 200) {
            commit('UPDATED_INTENT', newIntent)
            resolve()
          } else {
            reject(response.message)
          }
        })
        .catch((err) => reject(err))
    })
  },
  deleteIntent({ commit }, opts) {
    const { botID, intentID } = opts
    return new Promise((resolve, reject) => {
      this.$axios({
        url: `/api/bots/${botID}/intents/${intentID}/`,
        method: 'delete'
      })
        .then((response) => {
          console.log('createIntent => response', response)
          if (response.code === 200) {
            commit('DELETED_INTENT', intentID)
            resolve()
          } else {
            reject(response.message)
          }
        })
        .catch((err) => reject(err))
    })
  },
  getEntities({ commit }, botID) {
    return new Promise((resolve, reject) => {
      this.$axios({
        url: `/api/bots/${botID}/entities/`,
        method: 'get'
      })
        .then((response) => {
          if (response.code === 200) {
            commit('SET_ENTITIES', response.data.items)
            resolve(response.data)
          } else {
            reject(response.message)
          }
        })
        .catch((error) => {
          reject(error)
        })
    })
  },
  createEntity({ commit }, opts) {
    const { botID, newEntity } = opts
    return new Promise((resolve, reject) => {
      this.$axios({
        url: `/api/bots/${botID}/entities/`,
        method: 'post',
        data: newEntity
      })
        .then((response) => {
          console.log('createEntity => response', response)
          if (response.code === 200) {
            commit('CREATED_ENTITY', response.data)
            resolve(response.data)
          } else {
            reject(response.message)
          }
        })
        .catch((err) => reject(err))
    })
  },
  updateEntity({ commit }, opts) {
    const { botID, newEntity } = opts
    return new Promise((resolve, reject) => {
      this.$axios({
        url: `/api/bots/${botID}/entities/${newEntity.id}/`,
        method: 'patch',
        data: newEntity
      })
        .then((response) => {
          console.log('updateEntity => response', response)
          if (response.code === 200) {
            commit('UPDATED_ENTITY', newEntity)
            resolve()
          } else {
            reject(response.message)
          }
        })
        .catch((err) => reject(err))
    })
  },
  deleteEntity({ commit }, opts) {
    const { botID, entityID } = opts
    return new Promise((resolve, reject) => {
      this.$axios({
        url: `/api/bots/${botID}/entities/${entityID}/`,
        method: 'delete'
      })
        .then((response) => {
          console.log('deleteEntity => response', response)
          if (response.code === 200) {
            commit('DELETED_ENTITY', entityID)
            resolve()
          } else {
            reject(response.message)
          }
        })
        .catch((err) => reject(err))
    })
  },
  getTerms({ commit }, opts) {
    const { botID, entityID } = opts
    return new Promise((resolve, reject) => {
      this.$axios({
        url: `/api/bots/${botID}/entities/${entityID}/terms/`,
        method: 'get'
      })
        .then((response) => {
          console.log('createTerm => response', response)
          if (response.code === 200) {
            resolve(response.data.items)
          } else {
            reject(response.message)
          }
        })
        .catch((error) => {
          reject(error)
        })
    })
  },
  createTerm({ commit }, opts) {
    const { botID, entityID, newTerm } = opts
    return new Promise((resolve, reject) => {
      this.$axios({
        url: `/api/bots/${botID}/entities/${entityID}/terms/`,
        method: 'post',
        data: newTerm
      })
        .then((response) => {
          console.log('createTerm => response', response)
          if (response.code === 200) {
            resolve(response.data)
          } else {
            reject(response.message)
          }
        })
        .catch((error) => {
          reject(error)
        })
    })
  },
  updateTerm({ commit }, opts) {
    const { botID, entityID, termID, newTerm } = opts
    return new Promise((resolve, reject) => {
      this.$axios({
        url: `/api/bots/${botID}/entities/${entityID}/terms/${termID}/`,
        method: 'patch',
        data: newTerm
      })
        .then((response) => {
          console.log('updateTerm => response', response)
          if (response.code === 200) {
            resolve(response.data)
          } else {
            reject(response.message)
          }
        })
        .catch((error) => {
          reject(error)
        })
    })
  },
  deleteTerm({ commit }, opts) {
    const { botID, entityID, termID } = opts
    return new Promise((resolve, reject) => {
      this.$axios({
        url: `/api/bots/${botID}/entities/${entityID}/terms/${termID}/`,
        method: 'delete'
      })
        .then((response) => {
          console.log('deleteTerm => response', response)
          if (response.code === 200) {
            resolve(response.data)
          } else {
            reject(response.message)
          }
        })
        .catch((error) => {
          reject(error)
        })
    })
  },
  getSamples({ commit }, opts) {
    const { botID, intentID, entityID } = opts
    return new Promise((resolve, reject) => {
      this.$axios({
        url: `/api/bots/${botID}/samples/`,
        method: 'get',
        params: {
          intent: intentID,
          entity: entityID
        }
      })
        .then((response) => {
          console.log('getSamples => response', response)
          if (response.code === 200) {
            resolve(response.data)
          } else {
            reject(response.message)
          }
        })
        .catch((error) => {
          reject(error)
        })
    })
  },
  addSample({ commit }, opts) {
    const { botID, sample } = opts
    return new Promise((resolve, reject) => {
      this.$axios({
        url: `/api/bots/${botID}/samples/`,
        method: 'post',
        data: sample
      })
        .then((response) => {
          console.log('addSample => response', response)
          if (response.code === 200) {
            resolve(response.data)
          } else {
            reject(response.message)
          }
        })
        .catch((error) => {
          reject(error)
        })
    })
  },
  updateSample({ commit }, opts) {
    const { botID, newSample } = opts
    return new Promise((resolve, reject) => {
      this.$axios({
        url: `/api/bots/${botID}/samples/${newSample.id}/`,
        method: 'patch',
        data: newSample
      })
        .then((response) => {
          console.log('updateSample => response', response)
          if (response.code === 200) {
            resolve(newSample)
          } else {
            reject(response.message)
          }
        })
        .catch((err) => reject(err))
    })
  },
  deleteSample({ commit }, opts) {
    const { botID, sampleID } = opts
    return new Promise((resolve, reject) => {
      this.$axios({
        url: `/api/bots/${botID}/samples/${sampleID}/`,
        method: 'delete'
      })
        .then((response) => {
          console.log('deleteSample => response', response)
          if (response.code === 200) {
            resolve(sampleID)
          } else {
            reject(response.message)
          }
        })
        .catch((err) => reject(err))
    })
  },
  addTag({ commit }, opts) {
    const { botID, sampleID, newTag } = opts
    return new Promise((resolve, reject) => {
      this.$axios({
        url: `/api/bots/${botID}/samples/${sampleID}/tags/`,
        method: 'post',
        data: newTag
      })
        .then((response) => {
          console.log('addTag => response', response)
          if (response.code === 200) {
            resolve(response.data)
          } else {
            reject(response.message)
          }
        })
        .catch((error) => {
          reject(error)
        })
    })
  },
  updateTag({ commit }, opts) {
    const { botID, sampleID, newTag } = opts
    return new Promise((resolve, reject) => {
      this.$axios({
        url: `/api/bots/${botID}/samples/${sampleID}/tags/${newTag.id}/`,
        method: 'patch',
        data: newTag
      })
        .then((response) => {
          console.log('updateTag => response', response)
          if (response.code === 200) {
            resolve(response.data)
          } else {
            reject(response.message)
          }
        })
        .catch((error) => {
          reject(error)
        })
    })
  },
  getAllSpamJourneyMessage({ commit }, opts) {
    const { botID } = opts
    return new Promise((resolve, reject) => {
      this.$axios({
        url: `/api_spam/bots/${botID}/spam-journey`,
        method: 'get'
      })
        .then((response) => {
          console.log('get all spam message => response', response)
          if (response.code === 0) {
            commit('SET_SPAM_JOURNEYS', response.data.items)
            resolve(response.data)
          } else {
            reject(response.message)
          }
        })
        .catch((error) => {
          reject(error)
        })
    })
  },
  deleteTag({ commit }, opts) {
    const { botID, sampleID, tagID } = opts
    return new Promise((resolve, reject) => {
      this.$axios({
        url: `/api/bots/${botID}/samples/${sampleID}/tags/${tagID}/`,
        method: 'delete'
      })
        .then((response) => {
          console.log('deleteTag => response', response)
          if (response.code === 200) {
            resolve(tagID)
          } else {
            reject(response.message)
          }
        })
        .catch((error) => {
          reject(error)
        })
    })
  },
  getAllSkills({ commit }, botID) {
    return new Promise((resolve, reject) => {
      this.$axios({
        url: `/api/bots/${botID}/skills/`,
        method: 'get'
      })
        .then((response) => {
          console.log('getAllSkills => response', response)
          if (response.code === 200) {
            commit('SET_SKILLS', response.data.items)
            resolve(response.data)
          } else {
            reject(response.message)
          }
        })
        .catch((error) => {
          reject(error)
        })
    })
  },
  createSkill({ commit }, opts) {
    const { botID, newSkill } = opts
    return new Promise((resolve, reject) => {
      this.$axios({
        url: `/api/bots/${botID}/skills/`,
        method: 'post',
        data: newSkill
      })
        .then((response) => {
          console.log('createSkill => response', response)
          if (response.code === 200) {
            commit('CREATED_SKILL', response.data)
            resolve(response.data)
          } else {
            reject(response.message)
          }
        })
        .catch((error) => {
          reject(error)
        })
    })
  },
  updateSkillGroup({ commit }, opts) {
    const { botID, newSkillGroup, scenarioID, skillActionGroupId } = opts
    return new Promise((resolve, reject) => {
      this.$axios({
        url: `/api/bots/${botID}/scenarios/${scenarioID}/skillgroup/${skillActionGroupId}/`,
        method: 'patch',
        data: newSkillGroup
      })
        .then((response) => {
          console.log('update skill group => response', response)
          if (response.code === 200) {
            resolve(response.data)
          } else if (response.code === 422) {
            resolve(response)
          } else {
            reject(response.message)
          }
        })
        .catch((error) => {
          reject(error)
        })
    })
  },
  updateSkill({ commit }, opts) {
    const { botID, newSkill } = opts
    return new Promise((resolve, reject) => {
      this.$axios({
        url: `/api/bots/${botID}/skills/${newSkill.id}/`,
        method: 'patch',
        data: newSkill
      })
        .then((response) => {
          console.log('updateSkill => response', response)
          if (response.code === 200) {
            commit('UPDATED_SKILL', newSkill)
            resolve(response.data)
          } else {
            reject(response.message)
          }
        })
        .catch((error) => {
          reject(error)
        })
    })
  },
  removeSkillGroup({ commit }, opts) {
    const { botID, scenarioID, skillActionGroupId } = opts
    return new Promise((resolve, reject) => {
      this.$axios({
        url: `/api/bots/${botID}/scenarios/${scenarioID}/skillgroup/${skillActionGroupId}/`,
        method: 'delete'
      })
        .then((response) => {
          console.log('remove skill group => response', response)
          if (response.code === 200) {
            resolve(response.data)
          } else {
            reject(response.message)
          }
        })
        .catch((error) => {
          reject(error)
        })
    })
  },
  deleteSkill({ commit }, opts) {
    const { botID, skillID } = opts
    return new Promise((resolve, reject) => {
      this.$axios({
        url: `/api/bots/${botID}/skills/${skillID}/`,
        method: 'delete'
      })
        .then((response) => {
          console.log('deleteSkill => response', response)
          if (response.code === 200) {
            commit('DELETED_SKILL', skillID)
            resolve(skillID)
          } else {
            reject(response.message)
          }
        })
        .catch((error) => {
          reject(error)
        })
    })
  },
  getActionGroups({ commit }, opts) {
    const { botID, skillID } = opts
    return new Promise((resolve, reject) => {
      this.$axios({
        url: `/api/bots/${botID}/skills/${skillID}/action_groups/`,
        method: 'get'
      })
        .then((response) => {
          console.log('getActionGroups => response', response)
          if (response.code === 200) {
            resolve(response.data.items)
          } else {
            reject(response.message)
          }
        })
        .catch((error) => {
          reject(error)
        })
    })
  },
  createActionGroup({ commit }, opts) {
    const { botID, skillID, actionGroup } = opts
    return new Promise((resolve, reject) => {
      this.$axios({
        url: `/api/bots/${botID}/skills/${skillID}/action_groups/`,
        method: 'post',
        data: actionGroup
      })
        .then((response) => {
          console.log('createActionGroup => response', response)
          if (response.code === 200) {
            resolve(response.data)
          } else {
            reject(response.message)
          }
        })
        .catch((error) => {
          reject(error)
        })
    })
  },
  updateActionGroup({ commit }, opts) {
    const { botID, skillID, groupID, actionGroup } = opts
    return new Promise((resolve, reject) => {
      this.$axios({
        url: `/api/bots/${botID}/skills/${skillID}/action_groups/${groupID}/`,
        method: 'patch',
        data: actionGroup
      })
        .then((response) => {
          console.log('updateActionGroup => response', response)
          if (response.code === 200) {
            resolve(response.data)
          } else {
            reject(response.message)
          }
        })
        .catch((error) => {
          reject(error)
        })
    })
  },
  deleteActionGroup({ commit }, opts) {
    const { botID, skillID, groupID } = opts
    return new Promise((resolve, reject) => {
      this.$axios({
        url: `/api/bots/${botID}/skills/${skillID}/action_groups/${groupID}/`,
        method: 'delete'
      })
        .then((response) => {
          console.log('deleteActionGroup => response', response)
          if (response.code === 200) {
            resolve(response)
          } else {
            reject(response.message)
          }
        })
        .catch((error) => {
          reject(error)
        })
    })
  },
  getActions({ commit }, opts) {
    const { botID, skillID, groupID } = opts
    return new Promise((resolve, reject) => {
      this.$axios({
        url: `/api/bots/${botID}/skills/${skillID}/action_groups/${groupID}/actions/`,
        method: 'get'
      })
        .then((response) => {
          console.log('getActions => response', response)
          if (response.code === 200) {
            resolve(response.data.items)
          } else {
            reject(response.message)
          }
        })
        .catch((error) => {
          reject(error)
        })
    })
  },
  createAction({ commit }, opts) {
    const { botID, skillID, groupID, newAction } = opts
    return new Promise((resolve, reject) => {
      this.$axios({
        url: `/api/bots/${botID}/skills/${skillID}/action_groups/${groupID}/actions/`,
        method: 'post',
        data: newAction
      })
        .then((response) => {
          console.log('createAction => response', response)
          if (response.code === 200) {
            resolve(response.data)
          } else {
            reject(response.message)
          }
        })
        .catch((error) => {
          reject(error)
        })
    })
  },
  updateAction({ commit }, opts) {
    const { botID, skillID, groupID, actionID, newAction } = opts
    return new Promise((resolve, reject) => {
      this.$axios({
        url: `/api/bots/${botID}/skills/${skillID}/action_groups/${groupID}/actions/${actionID}/`,
        method: 'patch',
        data: newAction
      })
        .then((response) => {
          console.log('updateAction => response', response)
          if (response.code === 200) {
            resolve(response.data)
          } else {
            reject(response.message)
          }
        })
        .catch((error) => {
          reject(error)
        })
    })
  },
  deleteAction({ commit }, opts) {
    const { botID, skillID, groupID, actionID } = opts
    return new Promise((resolve, reject) => {
      this.$axios({
        url: `/api/bots/${botID}/skills/${skillID}/action_groups/${groupID}/actions/${actionID}/`,
        method: 'delete'
      })
        .then((response) => {
          console.log('delete => response', response)
          if (response.code === 200) {
            resolve(response)
          } else {
            reject(response.message)
          }
        })
        .catch((error) => {
          reject(error)
        })
    })
  },
  getAllChannels({ commit }, botID) {
    return new Promise((resolve, reject) => {
      this.$axios({
        url: `/api/bots/${botID}/channels/`,
        method: 'get'
      })
        .then((response) => {
          console.log('getAllChannels => response', response)
          if (response.code === 200) {
            commit('SET_CHANNELS', response.data.items)
            resolve(response.data)
          } else {
            reject(response.message)
          }
        })
        .catch((error) => {
          reject(error)
        })
    })
  },
  getPagesInfo({ commit }, opts) {
    const { botID, fbToken } = opts
    return new Promise((resolve, reject) => {
      this.$axios({
        url: `/api/bots/${botID}/channels/fb_get_token/`,
        method: 'post',
        data: fbToken
      })
        .then((response) => {
          console.log('prefetch channels => response', response)
          if (response.code === 200) {
            resolve(response.data)
          } else {
            reject(response.message)
          }
        })
        .catch((error) => {
          reject(error)
        })
    })
  },
  createFBChannel({ commit }, opts) {
    const { botID, newChannel } = opts
    return new Promise((resolve, reject) => {
      this.$axios({
        url: `/api/bots/${botID}/channels/`,
        method: 'post',
        data: newChannel
      })
        .then((response) => {
          console.log('add new channel => response', response)
          if (response.code === 200) {
            commit('CREATED_CHANNEL', response.data)
            resolve(response.data)
          } else {
            reject(response.message)
          }
        })
        .catch((error) => {
          reject(error)
        })
    })
  },
  deleteChannel({ commit }, opts) {
    const { botID, channelID } = opts
    return new Promise((resolve, reject) => {
      this.$axios({
        url: `/api/bots/${botID}/channels/${channelID}/`,
        method: 'delete'
      })
        .then((response) => {
          console.log('add new channel => response', response)
          if (response.code === 200) {
            commit('DELETED_CHANNEL', channelID)
            resolve(response.data)
          } else {
            reject(response.message)
          }
        })
        .catch((error) => {
          reject(error)
        })
    })
  },
  getFaqs({ commit }, opts) {
    const { botID } = opts
    return new Promise((resolve, reject) => {
      this.$axios({
        url: `/api/bots/${botID}/faqs/`,
        method: 'get'
      })
        .then((response) => {
          console.log('get faqs => response', response)
          if (response.code === 200) {
            commit('SET_FAQS', response.data.items)
            resolve(response.data)
          } else {
            reject(response.message)
          }
        })
        .catch((error) => {
          reject(error)
        })
    })
  },
  createFaq({ commit }, opts) {
    const { botID, newFaq } = opts
    return new Promise((resolve, reject) => {
      this.$axios({
        url: `/api/bots/${botID}/faqs/`,
        method: 'post',
        data: newFaq
      })
        .then((response) => {
          console.log('add new faqs => response', response)
          if (response.code === 200) {
            commit('CREATED_FAQ', response.data)
            resolve(response.data)
          } else {
            reject(response.message)
          }
        })
        .catch((error) => {
          reject(error)
        })
    })
  },
  getDetailFaq({ commit }, opts) {
    const { botID, faqID } = opts
    return new Promise((resolve, reject) => {
      this.$axios({
        url: `/api/bots/${botID}/faqs/${faqID}/`,
        method: 'get'
      })
        .then((response) => {
          console.log('get detail faq => response', response)
          if (response.code === 200) {
            commit('GET_FAQ', response.data.item)
            resolve(response.data)
          } else {
            reject(response.message)
          }
        })
        .catch((error) => {
          reject(error)
        })
    })
  },
  updateFaq({ commit }, opts) {
    const { botID, faqID, newFaq } = opts
    return new Promise((resolve, reject) => {
      this.$axios({
        url: `/api/bots/${botID}/faqs/${faqID}/`,
        method: 'patch',
        data: newFaq
      })
        .then((response) => {
          console.log('update faq => response', response)
          if (response.code === 200) {
            commit('UPDATED_FAQ', response.data)
            resolve(response.data)
          } else {
            reject(response.message)
          }
        })
        .catch((error) => {
          reject(error)
        })
    })
  },
  deleteFaq({ commit }, opts) {
    const { botID, faqID } = opts
    return new Promise((resolve, reject) => {
      this.$axios({
        url: `/api/bots/${botID}/faqs/${faqID}/`,
        method: 'delete'
      })
        .then((response) => {
          console.log('deleteFaq => response', response)
          if (response.code === 200) {
            commit('DELETED_FAQ', faqID)
            resolve(faqID)
          } else {
            reject(response.message)
          }
        })
        .catch((error) => {
          reject(error)
        })
    })
  },
  importEntity({ commit }, opts) {
    const { botID, entityID, data } = opts
    return new Promise((resolve, reject) => {
      this.$axios({
        url: `/api/bots/${botID}/entities/${entityID}/data/upload/`,
        method: 'post',
        data
      })
        .then((response) => {
          if (response.code === 200) {
            resolve(response)
          } else {
            reject(response.message)
          }
        })
        .catch((error) => {
          reject(error)
        })
    })
  },
  exportEntity({ commit }, opts) {
    const { botID, entityID } = opts
    return new Promise((resolve, reject) => {
      this.$axios({
        url: `/api/bots/${botID}/entities/${entityID}/data/download/`,
        method: 'get',
        responseType: 'blob'
      })
        .then((response) => {
          const url = window.URL.createObjectURL(new Blob([response]))
          const link = document.createElement('a')
          link.href = url
          link.setAttribute('download', 'file.json') // or any other extension
          document.body.appendChild(link)
          link.click()
        })
        .catch((error) => {
          reject(error)
        })
    })
  },
  syncEntityWithStore({ commit }, opts) {
    const { botID, entityID, storeID, storeField } = opts
    return new Promise((resolve, reject) => {
      this.$axios({
        url: `/api/bots/${botID}/entities/${entityID}/sync_store/`,
        method: 'post',
        data: {
          store_id: storeID,
          store_field_code: storeField
        }
      })
        .then((response) => {
          if (response.code === 200) {
            resolve(response)
          } else {
            reject(response)
          }
        })
        .catch((error) => {
          reject(error)
        })
    })
  },
  importIntent({ commit }, opts) {
    const { botID, intentID, data } = opts
    return new Promise((resolve, reject) => {
      this.$axios({
        url: `/api/bots/${botID}/intents/${intentID}/data/upload/`,
        method: 'post',
        data
      })
        .then((response) => {
          if (response.code === 200) {
            resolve(response)
          } else {
            reject(response.message)
          }
        })
        .catch((error) => {
          reject(error)
        })
    })
  },
  exportIntent({ commit }, opts) {
    const { botID, intentID } = opts
    return new Promise((resolve, reject) => {
      this.$axios({
        url: `/api/bots/${botID}/intents/${intentID}/data/download/`,
        method: 'get',
        responseType: 'blob'
      })
        .then((response) => {
          const url = window.URL.createObjectURL(new Blob([response]))
          const link = document.createElement('a')
          link.href = url
          link.setAttribute('download', 'file.json') // or any other extension
          document.body.appendChild(link)
          link.click()
        })
        .catch((error) => {
          reject(error)
        })
    })
  },

  // scenarios
  getScenarios({ commit }, opts) {
    const { botID } = opts
    return new Promise((resolve, reject) => {
      this.$axios({
        url: `/api/bots/${botID}/scenarios/`,
        method: 'get'
      })
        .then((response) => {
          console.log('getAllBots => response', response)
          if (response.code === 200) {
            commit('SET_SCENARIOS', response.data.items)
            resolve(response.data)
          } else {
            reject(response.message)
          }
        })
        .catch((error) => {
          reject(error)
        })
    })
  },
  createScenario({ commit }, opts) {
    const { botID, newScenario } = opts
    return new Promise((resolve, reject) => {
      this.$axios({
        url: `/api/bots/${botID}/scenarios/`,
        method: 'post',
        data: newScenario
      })
        .then((response) => {
          console.log('getAllBots => response', response)
          if (response.code === 200) {
            commit('CREATE_SCENARIOS', response.data)
            resolve(response)
          } else if (response.code === 422) {
            resolve(response)
          } else {
            reject(response.message)
          }
        })
        .catch((error) => {
          reject(error)
        })
    })
  },
  updateScenario({ commit }, opts) {
    const { botID, newScenario, scenarioID } = opts
    return new Promise((resolve, reject) => {
      this.$axios({
        url: `/api/bots/${botID}/scenarios/${scenarioID}/`,
        method: 'patch',
        data: newScenario
      })
        .then((response) => {
          console.log('getAllBots => response', response)
          if (response.code === 200) {
            resolve(response)
          } else if (response.code === 422) {
            resolve(response)
          } else {
            reject(response.message)
          }
        })
        .catch((error) => {
          reject(error)
        })
    })
  },
  cloneScenario({ commit }, opts) {
    const { botID, data, scenrioID } = opts
    return new Promise((resolve, reject) => {
      this.$axios({
        url: `/api/bots/${botID}/scenarios/${scenrioID}/clone/`,
        method: 'post',
        data
      })
        .then((response) => {
          console.log('cloneScenario => response', response)
          if (response.code === 200) {
            resolve(response.data)
          } else if (response.code === 422) {
            resolve(response)
          } else {
            reject(response.message)
          }
        })
        .catch((error) => {
          reject(error)
        })
    })
  },
  removeScenario({ commit }, opts) {
    const { botID, scenrioID } = opts
    return new Promise((resolve, reject) => {
      this.$axios({
        url: `/api/bots/${botID}/scenarios/${scenrioID}/`,
        method: 'delete'
      })
        .then((response) => {
          console.log('remove scenarios => response', response)
          if (response.code === 200 || response.code === 400) {
            resolve(response)
          } else {
            reject(response.message)
          }
        })
        .catch((error) => {
          reject(error)
        })
    })
  },
  getScenario({ commit }, opts) {
    const { botID, scenarioID } = opts
    return new Promise((resolve, reject) => {
      this.$axios({
        url: `/api/bots/${botID}/scenarios/${scenarioID}/`,
        method: 'get'
      })
        .then((response) => {
          console.log('get scenarios => response', response)
          if (response.code === 200) {
            resolve(response.data)
          } else {
            reject(response.message)
          }
        })
        .catch((error) => {
          reject(error)
        })
    })
  },

  // skill group
  getSkillGroup({ commit }, opts) {
    const { botID, scenarioID } = opts
    return new Promise((resolve, reject) => {
      this.$axios({
        url: `/api/bots/${botID}/scenarios/${scenarioID}/skillgroup/`,
        method: 'get'
      })
        .then((response) => {
          console.log('getAll skill group => response', response)
          if (response.code === 200) {
            commit('SET_SKILL_GROUP', response.data.items)
            resolve(response.data)
          } else {
            reject(response.message)
          }
        })
        .catch((error) => {
          reject(error)
        })
    })
  },
  updateSkillScenario({ commit }, opts) {
    const { botID, skillID, scenarioID, scenarioGroupSkillID, newSkill } = opts
    return new Promise((resolve, reject) => {
      this.$axios({
        url: `/api/bots/${botID}/scenarios/${scenarioID}/skillgroup/${scenarioGroupSkillID}/skills/${skillID}/`,
        method: 'patch',
        data: newSkill
      })
        .then((response) => {
          console.log('update skill', response)
          if (response.code === 200) {
            resolve(response.data)
          } else {
            reject(response.message)
          }
        })
        .catch((error) => {
          reject(error)
        })
    })
  },
  createSkillGroup({ commit }, opts) {
    const { botID, scenarioID, newSkillGroup } = opts
    return new Promise((resolve, reject) => {
      this.$axios({
        url: `/api/bots/${botID}/scenarios/${scenarioID}/skillgroup/`,
        method: 'post',
        data: newSkillGroup
      })
        .then((response) => {
          console.log('create skill group => response', response)
          if (response.code === 200) {
            resolve(response.data)
          } else if (response.code === 422) {
            resolve(response)
          } else {
            reject(response.message)
          }
        })
        .catch((error) => {
          reject(error)
        })
    })
  },

  // skill in skill group in scenarios
  createSkillInSkillGroup({ commit }, opts) {
    const { botID, scenarioID, scenarioGroupSkillID, newSkill } = opts
    return new Promise((resolve, reject) => {
      this.$axios({
        url: `/api/bots/${botID}/scenarios/${scenarioID}/skillgroup/${scenarioGroupSkillID}/skills/`,
        method: 'post',
        data: newSkill
      })
        .then((response) => {
          console.log('create skill', response)
          if (response.code === 200) {
            commit('CREATED_SKILL', response.data)
            resolve(response.data)
          } else {
            reject(response.message)
          }
        })
        .catch((error) => {
          reject(error)
        })
    })
  },
  removeSkillInSkillGroup({ commit }, opts) {
    const { botID, skillID, scenarioID, scenarioGroupSkillID } = opts
    return new Promise((resolve, reject) => {
      this.$axios({
        url: `/api/bots/${botID}/scenarios/${scenarioID}/skillgroup/${scenarioGroupSkillID}/skills/${skillID}/`,
        method: 'delete'
      })
        .then((response) => {
          console.log('remove skill', response)
          if (response.code === 200) {
            resolve(response.data)
          } else {
            reject(response.message)
          }
        })
        .catch((error) => {
          reject(error)
        })
    })
  },
  getAllSheet({ commit }, opts) {
    const { botID } = opts
    return new Promise((resolve, reject) => {
      this.$axios({
        url: `/api_store/bots/${botID}/sheets/gsheet-query/`,
        method: 'get'
      })
        .then((response) => {
          if (response.code === 0) {
            resolve(response.data)
          } else {
            reject(response.message)
          }
        })
        .catch((error) => {
          reject(error)
        })
    })
  },
  getAllTestCase({ commit }, opts) {
    const { botID } = opts
    return new Promise((resolve, reject) => {
      this.$axios({
        url: `/api_auto_test/bots/${botID}/test-case/`,
        method: 'get'
      })
        .then((response) => {
          if (response.code === 0) {
            commit('SET_TEST_CASES', response.data)
            resolve(response.data)
          } else {
            reject(response.message)
          }
        })
        .catch((error) => {
          reject(error)
        })
    })
  },
  createTestCase({ commit }, opts) {
    const { botID, newTestCase } = opts
    return new Promise((resolve, reject) => {
      this.$axios({
        url: `/api_auto_test/bots/${botID}/test-case/`,
        method: 'put',
        data: {
          name: newTestCase.name,
          description: newTestCase.description
        }
      })
        .then((response) => {
          if (response.code === 0) {
            resolve(response.data)
          } else {
            reject(response.message)
          }
        })
        .catch((error) => {
          reject(error)
        })
    })
  },
  updateTestCase({ commit }, opts) {
    const { botID, newTestCase } = opts
    return new Promise((resolve, reject) => {
      this.$axios({
        url: `/api_auto_test/bots/${botID}/test-case/${newTestCase.id}/`,
        method: 'put',
        data: {
          name: newTestCase.name,
          description: newTestCase.description
        }
      })
        .then((response) => {
          if (response.code === 0) {
            resolve(response.data)
          } else {
            reject(response.message)
          }
        })
        .catch((error) => {
          reject(error)
        })
    })
  },
  deleteTestCase({ commit }, opts) {
    const { botID, id } = opts
    return new Promise((resolve, reject) => {
      this.$axios({
        url: `/api_auto_test/bots/${botID}/test-case/${id}/`,
        method: 'delete'
      })
        .then((response) => {
          if (response.code === 0) {
            resolve(response.data)
          } else {
            reject(response.message)
          }
        })
        .catch((error) => {
          reject(error)
        })
    })
  },
  getDetailTestCase({ commit }, opts) {
    const { botID, testCaseId } = opts
    return new Promise((resolve, reject) => {
      this.$axios({
        url: `/api_auto_test/bots/${botID}/test-case/${testCaseId}/`,
        method: 'get'
      })
        .then((response) => {
          if (response.code === 0) {
            resolve(response.data)
          } else {
            reject(response.message)
          }
        })
        .catch((error) => {
          reject(error)
        })
    })
  },
  createTurn({ commit }, opts) {
    const { botID, testCaseId, newTurn } = opts
    return new Promise((resolve, reject) => {
      this.$axios({
        url: `/api_auto_test/bots/${botID}/test-case/${testCaseId}/turn/`,
        method: 'put',
        data: newTurn
      })
        .then((response) => {
          if (response.code === 0) {
            resolve(response.data)
          } else {
            reject(response.message)
          }
        })
        .catch((error) => {
          reject(error)
        })
    })
  },
  editTurn({ commit }, opts) {
    const { botID, testCaseId, newTurn } = opts
    return new Promise((resolve, reject) => {
      this.$axios({
        url: `/api_auto_test/bots/${botID}/test-case/${testCaseId}/turn/${newTurn.turn_id}/`,
        method: 'put',
        data: newTurn
      })
        .then((response) => {
          if (response.code === 0) {
            resolve(response.data)
          } else {
            reject(response.message)
          }
        })
        .catch((error) => {
          reject(error)
        })
    })
  },
  editPosition({ commit }, opts) {
    const { botID, testCaseId, turnId, position } = opts
    return new Promise((resolve, reject) => {
      this.$axios({
        url: `/api_auto_test/bots/${botID}/test-case/${testCaseId}/turn/${turnId}/`,
        method: 'patch',
        data: {
          position
        }
      })
        .then((response) => {
          if (response.code === 0) {
            resolve(response.data)
          } else {
            reject(response.message)
          }
        })
        .catch((error) => {
          reject(error)
        })
    })
  },
  trainTurn({ commit }, opts) {
    const { botID, testCaseId, turnId } = opts
    return new Promise((resolve, reject) => {
      this.$axios({
        url: `/api_auto_test/bots/${botID}/test-case/${testCaseId}/turn/${turnId}/`,
        method: 'post'
      })
        .then((response) => {
          if (response.code === 0) {
            resolve(response.data)
          } else {
            reject(response.message)
          }
        })
        .catch((error) => {
          reject(error)
        })
    })
  },
  deleteTurn({ commit }, opts) {
    const { botID, testCaseId, turnId } = opts
    return new Promise((resolve, reject) => {
      this.$axios({
        url: `/api_auto_test/bots/${botID}/test-case/${testCaseId}/turn/${turnId}/`,
        method: 'delete'
      })
        .then((response) => {
          if (response.code === 0) {
            resolve(response.data)
          } else {
            reject(response.message)
          }
        })
        .catch((error) => {
          reject(error)
        })
    })
  },
  runOneTest({ commit }, opts) {
    const { botID, testCaseId } = opts
    return new Promise((resolve, reject) => {
      this.$axios({
        url: `/api_auto_test/bots/${botID}/test-case/${testCaseId}/`,
        method: 'post'
      })
        .then((response) => {
          if (response.code === 0) {
            resolve(response.data)
          } else {
            reject(response.message)
          }
        })
        .catch((error) => {
          reject(error)
        })
    })
  },

  runAllTest({ commit }, opts) {
    const { botID } = opts
    return new Promise((resolve, reject) => {
      this.$axios({
        url: `/api_auto_test/bots/${botID}/test-case/`,
        method: 'post'
      })
        .then((response) => {
          if (response.code === 0) {
            resolve(response.data)
          } else {
            reject(response.message)
          }
        })
        .catch((error) => {
          reject(error)
        })
    })
  },
  exportTurn({ commit }, opts) {
    const { botID, testCaseId, type, name } = opts
    return new Promise((resolve, reject) => {
      this.$axios({
        url: `/api_auto_test/bots/${botID}/test-case/${testCaseId}/transfer/`,
        method: 'get',
        responseType: 'blob',
        params: {
          ext: type
        }
      })
        .then((response) => {
          const url = window.URL.createObjectURL(new Blob([response]))
          const link = document.createElement('a')
          link.href = url
          link.setAttribute('download', `${name}.csv`) // or any other extension
          document.body.appendChild(link)
          link.click()
          resolve(response)
        })
        .catch((error) => {
          reject(error)
        })
    })
  },
  importTurn({ commit }, opts) {
    const { botID, testCaseId, data } = opts
    return new Promise((resolve, reject) => {
      this.$axios({
        url: `/api_auto_test/bots/${botID}/test-case/${testCaseId}/transfer/`,
        method: 'post',
        data,
        headers: {
          'Content-Type': 'text/csv'
        }
      })
        .then((response) => {
          if (response.code === 0) {
            resolve(response)
          } else {
            reject(response.message)
          }
        })
        .catch((error) => {
          reject(error)
        })
    })
  },
  cloneSkillGroup({ commit }, opts) {
    const { botID, scenarioID, id, data } = opts
    return new Promise((resolve, reject) => {
      this.$axios({
        url: `/api/bots/${botID}/scenarios/${scenarioID}/skillgroup/${id}/clone/`,
        method: 'post',
        data
      })
        .then((response) => {
          if (response.code === 200) {
            resolve(response)
          } else {
            reject(response.message)
          }
        })
        .catch((error) => {
          reject(error)
        })
    })
  },
  getAnalyticFirst({ commit }, opts) {
    const { botID } = opts
    return new Promise((resolve, reject) => {
      this.$axios({
        url: `api_crm/bots/${botID}/customers/analytic`,
        method: 'post'
      })
        .then((response) => {
          if (response.code === 0) {
            resolve(response.data)
          } else {
            reject(response.message)
          }
        })
        .catch((error) => {
          reject(error)
        })
    })
  },
  sortAction({ commit }, opts) {
    const { botID, skillID, groupID, newAction } = opts
    return new Promise((resolve, reject) => {
      this.$axios({
        url: `/api/bots/${botID}/skills/${skillID}/action_groups/${groupID}/actions/sort/`,
        method: 'put',
        data: newAction
      })
        .then((response) => {
          if (response.code === 200) {
            resolve(response.data)
          } else {
            reject(response.message)
          }
        })
        .catch((error) => {
          reject(error)
        })
    })
  },
  sortSkill({ commit }, opts) {
    const { botID, skillID, newSkill } = opts
    return new Promise((resolve, reject) => {
      this.$axios({
        url: `/api/bots/${botID}/skills/${skillID}/action_groups/sort/`,
        method: 'put',
        data: newSkill
      })
        .then((response) => {
          if (response.code === 200) {
            resolve(response.data)
          } else if (response.code === 422) {
            resolve(response)
          } else {
            reject(response.message)
          }
        })
        .catch((error) => {
          reject(error)
        })
    })
  },
  getSystemEntity({ commit }) {
    return new Promise((resolve, reject) => {
      this.$axios({
        url: `/api/system-entities/`,
        method: 'get'
      })
        .then((response) => {
          if (response.code === 200) {
            commit('SET_SYSTEM_ENTITY', response.data.items)
            resolve(response.data)
          } else {
            reject(response.message)
          }
        })
        .catch((error) => {
          reject(error)
        })
    })
  },
  cloneSkill({ commit }, opts) {
    const { botID, scenarioID, scenarioGroupSkillID, skillID, data } = opts
    return new Promise((resolve, reject) => {
      this.$axios({
        url: `/api/bots/${botID}/scenarios/${scenarioID}/skillgroup/${scenarioGroupSkillID}/skills/${skillID}/clone/`,
        method: 'post',
        data
      })
        .then((response) => {
          console.log('clone skill', response)
          if (response.code === 200) {
            resolve(response.data)
          } else if (response.code === 422) {
            resolve(response)
          } else {
            reject(response.message)
          }
        })
        .catch((error) => {
          reject(error)
        })
    })
  },
  // customer
  getAllCustomersCrm({ commit }, opts) {
    const { botID, query, page, size } = opts
    return new Promise((resolve, reject) => {
      this.$axios({
        url: `api_crm/bots/${botID}/customers/?q=${query}`,
        method: 'get',
        params: {
          page,
          size
        }
      })
        .then((response) => {
          if (response.code === 0) {
            resolve(response.data)
          } else {
            reject(response.message)
          }
        })
        .catch((error) => {
          reject(error)
        })
    })
  },
  getSourceCrm({ commit }, opts) {
    const { botID } = opts
    return new Promise((resolve, reject) => {
      this.$axios({
        url: `api_crm/bots/${botID}/sources/`,
        method: 'get'
      })
        .then((response) => {
          if (response.code === 0) {
            commit('SET_SOURCE', response.data)
            resolve(response.data)
          } else {
            reject(response.message)
          }
        })
        .catch((error) => {
          reject(error)
        })
    })
  },
  postCustomerCrm({ commit }, opts) {
    const { botID, data } = opts
    return new Promise((resolve, reject) => {
      this.$axios({
        url: `api_crm/bots/${botID}/customers/${data.id}`,
        method: 'put',
        data
      })
        .then((response) => {
          if (response.code === 0) {
            resolve(response.data)
          } else {
            reject(response.message)
          }
        })
        .catch((error) => {
          reject(error)
        })
    })
  },
  saveSegment({ commit }, opts) {
    const { botID, data } = opts
    return new Promise((resolve, reject) => {
      this.$axios({
        url: `api_crm/bots/${botID}/segments/`,
        method: 'post',
        data
      })
        .then((response) => {
          if (response.code === 0) {
            resolve(response.data)
          } else {
            reject(response.message)
          }
        })
        .catch((error) => {
          reject(error)
        })
    })
  },
  getSegments({ commit }, opts) {
    const { botID, page, size } = opts
    return new Promise((resolve, reject) => {
      this.$axios({
        url: `api_crm/bots/${botID}/segments/`,
        method: 'get',
        params: {
          page,
          size
        }
      })
        .then((response) => {
          if (response.code === 0) {
            resolve(response.data)
          } else {
            reject(response.message)
          }
        })
        .catch((error) => {
          reject(error)
        })
    })
  },
  deleteSegment({ commit }, opts) {
    const { botID, segmentId } = opts
    return new Promise((resolve, reject) => {
      this.$axios({
        url: `api_crm/bots/${botID}/segments/${segmentId}`,
        method: 'delete'
      })
        .then((response) => {
          if (response.code === 0) {
            resolve(response.data)
          } else {
            reject(response.message)
          }
        })
        .catch((error) => {
          reject(error)
        })
    })
  },
  countSegment({ commit }, opts) {
    const { params, botID } = opts
    return new Promise((resolve, reject) => {
      this.$axios({
        url: `api_crm/bots/${botID}/customers/count?${params}`,
        method: 'get'
      })
        .then((response) => {
          if (response.code === 0) {
            resolve(response.data)
          } else {
            reject(response.message)
          }
        })
        .catch((error) => {
          reject(error)
        })
    })
  },
  updateSegment({ commit }, opts) {
    const { botID, data, segmentId } = opts
    return new Promise((resolve, reject) => {
      this.$axios({
        url: `api_crm/bots/${botID}/segments/${segmentId}`,
        method: 'put',
        data
      })
        .then((response) => {
          if (response.code === 0) {
            resolve(response.data)
          } else {
            reject(response.message)
          }
        })
        .catch((error) => {
          reject(error)
        })
    })
  },
  importCrm({ commit }, opts) {
    const { data, botID } = opts
    return new Promise((resolve, reject) => {
      this.$axios({
        url: `/api_crm/bots/${botID}/customers/transfer`,
        method: 'post',
        data,
        headers: { 'content-type': 'multipart/form-data' }
      })
        .then((response) => {
          if (response.code === 0) {
            resolve(response)
          } else {
            resolve(response)
          }
        })
        .catch((error) => {
          reject(error)
        })
    })
  },
  quickSearchCustomer({ commit }, params) {
    return new Promise((resolve, reject) => {
      this.$axios({
        url: `/api_crm/bots/${params.botID}/customers/search`,
        method: 'get',
        params
      })
        .then((response) => {
          if (response.code === 0) {
            resolve(response.data)
          } else {
            reject(response.message)
          }
        })
        .catch((error) => {
          reject(error)
        })
    })
  },
  exportExample({ commit }, opts) {
    const { botID } = opts
    return new Promise((resolve, reject) => {
      this.$axios({
        url: `/api_crm/bots/${botID}/customers/transfer`,
        method: 'get',
        params: {
          is_example: 1
        },
        responseType: 'blob'
      })
        .then((response) => {
          const url = window.URL.createObjectURL(new Blob([response]))
          const link = document.createElement('a')
          link.href = url
          link.setAttribute('download', 'du_lieu_mau.xlsx') // or any other extension
          document.body.appendChild(link)
          link.click()
          resolve(response)
        })
        .catch((error) => {
          reject(error)
        })
    })
  },
  // automation comment
  getAllChannelPosts({ commit }, opts) {
    return new Promise((resolve, reject) => {
      this.$axios({
        url: `/api_autocomment/bots/${opts}/channels/`,
        method: 'get'
      })
        .then((response) => {
          if (response.code === 0) {
            commit('SET_CHANNELS_POSTS', response.data.items)
            resolve(response.data)
          } else {
            reject(response.message)
          }
        })
        .catch((error) => {
          reject(error)
        })
    })
  },
  // store
  getAllStore({ commit }, opts) {
    return new Promise((resolve, reject) => {
      this.$axios({
        url: `/api_store/bots/${opts.botID}/store/`,
        method: 'get'
      })
        .then((response) => {
          if (response.code === 0) {
            resolve(response.data)
            commit('SET_STORE', response.data)
          } else {
            reject(response.message)
          }
        })
        .catch((error) => {
          reject(error)
        })
    })
  },
  createStore({ commit }, opts) {
    const { data } = opts
    return new Promise((resolve, reject) => {
      this.$axios({
        url: `/api_store/bots/${opts.botID}/store/`,
        method: 'post',
        data
      })
        .then((response) => {
          if (response.code === 0) {
            console.log(response.data)
            resolve(response.data)
          } else {
            reject(response.message)
          }
        })
        .catch((error) => {
          reject(error)
        })
    })
  },
  deleteStore({ commit }, opts) {
    return new Promise((resolve, reject) => {
      this.$axios({
        url: `/api_store/bots/${opts.botID}/store/${opts.storeID}/`,
        method: 'delete'
      })
        .then((response) => {
          if (response.code === 0) {
            resolve(response)
          } else {
            reject(response.message)
          }
        })
        .catch((error) => {
          reject(error)
        })
    })
  },
  getDetailStore({ commit }, opts) {
    return new Promise((resolve, reject) => {
      this.$axios({
        url: `/api_store/bots/${opts.botID}/store/${opts.storeID}/`,
        method: 'get'
      })
        .then((response) => {
          if (response.code === 0) {
            resolve(response.data)
          } else {
            reject(response.message)
          }
        })
        .catch((error) => {
          reject(error)
        })
    })
  },
  updateStore({ commit }, opts) {
    const { data } = opts
    return new Promise((resolve, reject) => {
      this.$axios({
        url: `/api_store/bots/${opts.botID}/store/${opts.storeID}/`,
        method: 'put',
        data
      })
        .then((response) => {
          if (response.code === 0) {
            resolve(response)
          } else {
            reject(response.message)
          }
        })
        .catch((error) => {
          reject(error)
        })
    })
  },
  createRecord({ commit }, opts) {
    const { data } = opts
    return new Promise((resolve, reject) => {
      this.$axios({
        url: `/api_store/bots/${opts.botID}/store/${opts.storeID}/record/`,
        method: 'post',
        data
      })
        .then((response) => {
          if (response.code === 0) {
            resolve(response)
          } else {
            reject(response.message)
          }
        })
        .catch((error) => {
          reject(error)
        })
    })
  },
  updateRecord({ commit }, opts) {
    const { data } = opts
    return new Promise((resolve, reject) => {
      this.$axios({
        url: `/api_store/bots/${opts.botID}/store/${opts.storeID}/record/${opts.recordID}/`,
        method: 'put',
        data
      })
        .then((response) => {
          if (response.code === 0) {
            resolve(response)
          } else {
            reject(response.message)
          }
        })
        .catch((error) => {
          reject(error)
        })
    })
  },
  exportStore({ commit }, opts) {
    return new Promise((resolve, reject) => {
      this.$axios({
        url: `/api_store/bots/${opts.botID}/store/${opts.storeID}/transfer/`,
        method: 'get',
        responseType: 'blob'
      })
        .then((response) => {
          const url = window.URL.createObjectURL(new Blob([response]))
          const link = document.createElement('a')
          link.href = url
          link.setAttribute('download', `${opts.storeName}.xlsx`) // or any other extension
          document.body.appendChild(link)
          link.click()
          resolve(response)
        })
        .catch((error) => {
          reject(error)
        })
    })
  },
  importStore({ commit }, opts) {
    const { data, botID } = opts
    return new Promise((resolve, reject) => {
      this.$axios({
        url: `/api_store/bots/${botID}/store/${opts.storeID}/transfer/`,
        method: 'post',
        data,
        headers: { 'content-type': 'multipart/form-data' }
      })
        .then((response) => {
          if (response.code === 0) {
            resolve(response)
          } else {
            resolve(response)
          }
        })
        .catch((error) => {
          reject(error)
        })
    })
  },
  deleteRecord({ commit }, opts) {
    const { data } = opts
    return new Promise((resolve, reject) => {
      this.$axios({
        url: `/api_store/bots/${opts.botID}/store/${opts.storeID}/record/`,
        method: 'delete',
        data
      })
        .then((response) => {
          if (response.code === 0) {
            resolve(response)
          } else {
            reject(response.message)
          }
        })
        .catch((error) => {
          reject(error)
        })
    })
  },
  getHistory({ commit }, opts) {
    const {
      botID,
      page,
      pageSize,
      isFilterUnknownIntent,
      isFilterFallbackAction,
      isFilterUnassignedMsg,
      intentFilters,
      fromTime,
      toTime
    } = opts
    const params = {}
    if (page) params.page = page
    if (pageSize) params.page_size = pageSize
    if (fromTime) params.from_time = fromTime
    if (toTime) params.to_time = toTime
    if (isFilterUnknownIntent) params.none_intent = true
    if (isFilterFallbackAction) params.fallback = true
    if (isFilterUnassignedMsg) params.not_labeled = true
    if (intentFilters) params.intents = intentFilters
    return new Promise((resolve, reject) => {
      this.$axios({
        url: `/api_conversation/conversation/history/bots/${botID}/messages`,
        method: 'get',
        params
      })
        .then((response) => {
          if (response.error_code === 0) {
            commit('SET_HISTORY', response.data)
            resolve(response)
          } else {
            reject(response.message)
          }
        })
        .catch((error) => {
          reject(error)
        })
    })
  },
  setHistoryMsgIntent({ commit }, opts) {
    const { botID, messageID, intent, intentType } = opts
    return new Promise((resolve, reject) => {
      this.$axios({
        url: `/api_conversation/conversation/history/bots/${botID}/messages/${messageID}`,
        method: 'put',
        data: {
          intent: {
            name: intent.name,
            intent_id: intent.id,
            intent_type: intentType
          }
        }
      })
        .then((response) => {
          if (response.error_code === 0) {
            resolve(response)
          } else {
            reject(response.message)
          }
        })
        .catch((error) => {
          reject(error)
        })
    })
  }
}
