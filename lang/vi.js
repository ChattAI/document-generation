export default {
  login: 'Đăng nhập',
  loginTitle: 'Đăng nhập vào hệ thống QL ChattAI',
  loginSuccess: 'Đăng nhập thành công',
  loginFailed: 'Đăng nhập không thành công',
  username: 'Tài khoản',
  usernameRequired: 'Chưa nhập tài khoản',
  password: 'Mật khẩu',
  password2: 'Nhập lại mật khẩu',
  passwordRequired: 'Chưa nhập mật khẩu',
  password2NotMatch: 'Nhập lại mật khẩu không khớp',
  forgotPassword: 'Bạn đã quên mật khẩu?',
  haveAccount: 'Bạn đã có tài khoản?',
  dontHaveAccount: 'Bạn chưa có tài khoản?',
  signup: 'Đăng ký',
  signupTitle: 'Đăng ký vào hệ thống QL ChattAI',
  signupSuccess: 'Đăng ký thành công',
  signupFailed: 'Đăng ký không thành công'
}
