export default {
  login: 'Log in',
  loginTitle: 'Log in for ChattAI Admin System',
  loginSuccess: 'Logged in for ChattAI Admin System',
  loginFailed: 'Log in failed',
  username: 'Username',
  usernameRequired: 'Username is required',
  password: 'Password',
  password2: 'Password confirm',
  passwordRequired: 'Password is required',
  password2NotMatch: 'Password confirm is not match',
  forgotPassword: 'Forgot your password?',
  haveAccount: 'Already have an account?',
  dontHaveAccount: "Don't have an account?",
  signup: 'Sign up',
  signupTitle: 'Sign up for ChattAI Admin System',
  signupSuccess: 'Signed up successful',
  signupFailed: 'Sign up failed'
}
