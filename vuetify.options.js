const colors = require('vuetify/es5/util/colors').default

export default {
  theme: {
    dark: false,
    themes: {
      light: {
        primary: '#047bf8',
        secondary: colors.pink.base,
        accent: colors.indigo.base,
        error: colors.red.base,
        warning: colors.amber.base,
        info: colors.cyan.base,
        success: colors.green.base
      }
    }
  },
  icons: {
    iconfont: 'mdi'
  },
  rtl: false
}
