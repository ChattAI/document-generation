export default {
  data() {
    return {
      relationshipList: [
        'Độc thân',
        'Hẹn hò',
        'Đã đính hôn',
        'Đã kết hôn',
        'Kết hôn đồng giới',
        'Chung sống',
        'Tìm hiểu',
        'Có mối quan hệ phức tạp',
        'Đã ly thân',
        'Đã ly hôn',
        'Góa'
      ],
      genderList: [
        {
          value: 'female',
          label: 'Nữ'
        },
        {
          value: 'male',
          label: 'Nam'
        },
        {
          value: 'other',
          label: 'Khác'
        }
      ],
      cityList: [
        'An Giang',
        'Bạc Liêu',
        'Bắc Cạn',
        'Bắc Giang',
        'Bắc Ninh',
        'Bến Tre',
        'Bình Dương',
        'Bình Định',
        'Bình Phước',
        'Bình Thuận',
        'Cà Mau',
        'Cao Bằng',
        'Cần Thơ',
        'Đà Nẵng',
        'Điện Biên',
        'Đắk Lắk',
        'Đắc Nông',
        'Đồng Nai',
        'Đồng Tháp',
        'Gia Lai',
        'Hà Giang',
        'Hà Nam',
        'Hà Tĩnh',
        'Hà Nội',
        'Hải Dương',
        'Hải Phòng',
        'Hậu Giang',
        'Hoà Bình',
        'Tp. Hồ Chí Minh',
        'Hưng Yên',
        'Khánh Hoà',
        'Kiên Giang',
        'Kon Tum',
        'Lai Châu',
        'Lạng Sơn',
        'Lào Cai',
        'Lâm Đồng',
        'Long An',
        'Nam Định',
        'Nghệ An',
        'Ninh Bình',
        'Ninh Thuận',
        'Phú Thọ',
        'Phú Yên',
        'Quảng Bình',
        'Quảng Nam',
        'Quảng Ngãi',
        'Quảng Ninh',
        'Quảng Trị',
        'Sóc Trăng',
        'Sơn La',
        'Tây Ninh',
        'Thái Bình',
        'Thái Nguyên',
        'Thanh Hoá',
        'Thừa Thiên Huế',
        'Tiền Giang',
        'Trà Vinh',
        'Tuyên Quang',
        'Vĩnh Long',
        'Vĩnh Phúc',
        'Yên Bái',
        'Bà Rịa Vũng Tàu'
      ]
    }
  }
}
