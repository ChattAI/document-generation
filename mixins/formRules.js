export default {
  data() {
    return {
      required: (v) => (!!v && !!v.trim()) || 'Infomaton require',
      phoneNumber: (v) => {
        if (!v) return true
        return (
          (/^[0-9|+]*$/g.test(v.trim()) && Math.abs(v)) ||
          'Phone number invalid'
        )
      },
      url: (v) => {
        if (!v) return true
        return (
          /^(https?|http):\/\/[^\s$.?#].[^\s]*$/g.test(v.trim()) ||
          'URL incorrect'
        )
      },
      email: (v) => {
        if (!v) return true
        // eslint-disable-next-line prettier/prettier
        return (
          /^[\w-\\.]+@([\w-]+\.)+[\w-]{2,4}$/.test(v) || 'Email incorrect'
        )
      },
      maxString: (v) => {
        if (!v) return true
        return v.length < 641 || 'Content too long'
      }
    }
  }
}
